﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using Ionic.Zip;

namespace ConsolePackDocumentOnly
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists("PackDocumentOnly_error.log"))
            {
                File.Delete("PackDocumentOnly_error.log");
            }

            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.docx", System.IO.SearchOption.AllDirectories);


            foreach (string docx in list)
            {
                turnBackDocumentXML(new FileInfo(docx));
            }
        }

        public static void turnBackDocumentXML(FileInfo docx)
        {
            string sourceFile = docx.FullName.Replace(".docx", ".xml");

            if (!File.Exists(sourceFile))
            {
                string error = " ** can't find unpacked xml file \r\nFile:\r\n" + sourceFile;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
                return;
            }

            FileInfo XmlFile = new FileInfo(sourceFile);


            if (XmlFile.Length < 1000)
            {
                string error = " ** invalid unpacked xml file \r\nFile:\r\n" + sourceFile;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
                XmlFile = null;
                return;
            }
            XmlFile = null;

            Program.unzipALL(docx);
 
            try
            {
                File.Copy(sourceFile, docx.FullName.Replace(".docx", "/word/document.xml"), true);
            }
            catch (Exception er)
            {
                string error = " - can't copy XML file \r\n File: \r\n" + sourceFile + "\r\nExeption:" + er.Message;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
            }


            Program.zipALL(docx);

             File.Delete(sourceFile);
        }

        public static  void zipALL(FileInfo docx)
        {
            string sourceFolder = docx.FullName.Replace(".docx", "");

            if (!Directory.Exists(sourceFolder))
            {
                string error = " ** can't find unpacked directory \r\nFile:\r\n" + docx.Name;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
                return;
            }

            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    try
                    {
                        zip.AddDirectory(sourceFolder);
                    }
                    catch (Exception er)
                    {
                        string error = " - adding files to pack failed!\r\nDirectory:" + sourceFolder + "\r\nExeption: " + er.Message;
                        File.AppendAllText("PackDocumentOnly_error.log", error);
                        Console.WriteLine(error);
                    }

                    try
                    {
                        zip.Save(docx.FullName);

                        Console.WriteLine(" - docx packed successfuly\r\n");

                         Program.removeDir(sourceFolder);
                    }
                    catch (Exception er3)
                    {
                        string error = " - can't save pack file\r\nDirectory:" + sourceFolder + "\r\nExeption: " + er3.Message;
                        File.AppendAllText("PackDocumentOnly_error.log", error);
                        Console.WriteLine(error);
                    }
                }
            }
            catch (Exception e)
            {
                string error = " - can't create zip file !\r\nFolder:" + sourceFolder + "\r\nExeption: " + e.Message;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
            }
        }

        public static void removeDir(string dirName)
        {
            Console.WriteLine(" - Tying to remove unpacked directory! (its may exists before)\r\n");
            System.IO.DirectoryInfo di = new DirectoryInfo(dirName);

            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception ex)
                {
                    string error = " *** can't delete a file in directory\r\nFile:" + file.Name + "\r\nException:" + ex.Message;
                    File.AppendAllText("PackDocumentOnly_error.log", error);
                    Console.WriteLine(error);
                }

            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete(true);
                }
                catch (Exception ex)
                {
                   string error=" *** can't delete a directory\r\nException:" + ex.Message;
                    File.AppendAllText("PackDocumentOnly_error.log", error);
                    Console.WriteLine(error);
                }

            }

            try
            {
                Directory.Delete(dirName, true);
            }
            catch (Exception)
            {

            }
        }

        public static void unzipALL(FileInfo docx)
        {

            string destFolder = docx.FullName.Replace(".docx", "");

            if (Directory.Exists(destFolder))
            {
                Program.removeDir(destFolder);
            }

            Console.WriteLine(" ** Trying to open " + docx.Name + "\r\n");

            try
            {
                /* استفاده از یوزینگ کمک میکند که فایلی باز نماند و پروسس بعد از اتمام امن بسته شود*/
                using (ZipFile zip = ZipFile.Read(docx.FullName))
                {
                    try
                    {
                        zip.ExtractAll(destFolder);
                        Console.WriteLine(" - docx unpacked successfuly\r\n\r\n");
                    }
                    catch (Exception er)
                    {
                        string error = " - unpack failed!\r\nFile:" + docx.FullName + "\r\nExeption: " + er.Message;
                        File.AppendAllText("PackDocumentOnly_error.log", error);
                        Console.WriteLine(error);
                    }
                }
            }
            catch (Exception e)
            {
                string error = " - can't open docx !\r\nFile:" + docx.FullName + "\r\nExeption: " + e.Message;
                File.AppendAllText("PackDocumentOnly_error.log", error);
                Console.WriteLine(error);
            }
        }
    }
}
