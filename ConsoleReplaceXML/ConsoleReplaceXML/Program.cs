﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace ConsoleReplaceXML
{
    class Program
    {
        static void Main(string[] args)
        {

            if (File.Exists("ReplaceXML_error.log"))
            {
                File.Delete("ReplaceXML_error.log");
            }


             string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.xml", System.IO.SearchOption.AllDirectories);

            string error;

            Regex RgxRemoveUnsigned9 = new Regex("<w:sz.*/>");

            Regex RgxRemoveUnsigned8 = new Regex(" <w:szCs.*/>");

            Regex RgxRemoveUnsigned7 = new Regex("<w:rFonts.*/>");

            Regex RgxRemoveUnsigned6 = new Regex("<w:highlight.*/>");

            Regex RgxRemoveUnsigned5 = new Regex("<w:rtl />");

            Regex RgxRemoveUnsigned4 = new Regex("<w:ltr />");

            Regex RgxRemoveUnsigned3 = new Regex("<w:bCs />");

            Regex RgxRemoveUnsigned2 = new Regex("<w:b />");

            Regex RgxRemoveUnsigned1 = new Regex("<w:lang.*/>");

            Regex RgxRemove0 = new Regex("<w:bidi />");

            Regex RgxRemove0O1 = new Regex("<w:rPr>[\r\n\t ]*</w:rPr>");

            Regex RgxRemove0O2 = new Regex("<w:pPr>[\r\n\t ]*</w:pPr>");


            Regex RgxRemove1 = new Regex("<w:pPr>[\r\n\t ]*<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*</w:pPr>");

            Regex RgxReplace2 = new Regex("<w:r>[\r\n\t ]*<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*<w:t xml:space=\"preserve\"> </w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace3 = new Regex("<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>");

            Regex RgxReplace4O1JustZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*<w:t xml:space=\"preserve\">(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace4JustZaban = new Regex("<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*<w:t xml:space=\"preserve\">(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace5O1JustZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*<w:t>(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace5JustZaban = new Regex("<w:rPr>[\r\n\t ]*<w:u w:val=\"single\" />[\r\n\t ]*</w:rPr>[\r\n\t ]*<w:t>(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace6O1JustZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:t xml:space=\"preserve\">(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace6JustZaban = new Regex("<w:r>[\r\n\t ]*<w:t xml:space=\"preserve\">(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace7O1JustZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:t>(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace7JustZaban = new Regex("<w:r>[\r\n\t ]*<w:t>(?=.*[آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ])(.*)</w:t>[\r\n\t ]*</w:r>");

            Regex RgxReplace8 = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:t");

            Regex RgxReplace9NotZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:rPr>");

            Regex RgxReplace10 = new Regex("<w:r>[\r\n\t ]*<w:t ");

            Regex RgxReplace11 = new Regex("<w:r>[\r\n\t ]*<w:t>");

            Regex RgxReplace12NotZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:object");

            Regex RgxReplace13NotZaban = new Regex("<w:p>[\r\n\t ]*<w:r>[\r\n\t ]*<w:drawing");

            Regex RgxReplace14 = new Regex("<w:sectPr[a-zA-Z0-9:/ \r\n\t<>=\"/]*w:sectPr>", RegexOptions.Multiline);


            /*Regex Ts1Fa1 = new Regex(">(\\(?الف[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts2Fa1 = new Regex(" (\\(?الف[ ]*(\\)|\\-|\\.|\\:))");

            Regex Ts3Fa2 = new Regex(">(\\(?ب[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts4Fa2 = new Regex(" (\\(?ب[ ]*(\\)|\\-|\\.|\\:))");

            Regex Ts5Fa3 = new Regex(">(\\(?پ[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts6Fa3 = new Regex(" (\\(?پ[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts7Fa3 = new Regex(">(\\(?ج[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts8Fa3 = new Regex(" (\\(?ج[ ]*(\\)|\\-|\\.|\\:))");

            Regex Ts9Fa4 = new Regex(">(\\(?ت[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts10Fa4 = new Regex(" (\\(?ت[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts11Fa4 = new Regex(">(\\(?د[ ]*(\\)|\\-|\\.|\\:))");
            Regex Ts12Fa4 = new Regex(" (\\(?د[ ]*(\\)|\\-|\\.|\\:))");


            Regex Ts1En1 = new Regex(">(\\(?[aA]{1}[ ]*(\\)|\\.|\\:))");
            Regex Ts2En1 = new Regex(" (\\(?[aA]{1}[ ]*(\\)|\\.|\\:))");

            Regex Ts3En2 = new Regex(">(\\(?[bB]{1}[ ]*(\\)|\\.|\\:))");
            Regex Ts4En2 = new Regex(" (\\(?[bB]{1}[ ]*(\\)|\\.|\\:))");

            Regex Ts5En3 = new Regex(">(\\(?[cC]{1}[ ]*(\\)|\\.|\\:))");
            Regex Ts6En3 = new Regex(" (\\(?[cC]{1}[ ]*(\\)|\\.|\\:))");

            Regex Ts7En4 = new Regex(">(\\(?[dD]{1}[ ]*(\\)|\\.|\\:))");
            Regex Ts8En4 = new Regex(" (\\(?[dD]{1}[ ]*(\\)|\\.|\\:))");*/




            foreach (string docx in list)
            {
                string Result = "";
                bool isTs = false;
                bool isZaban = false;
                bool isArabi = false;
                bool isOther = false;

                try
                {
                    Result = File.ReadAllText(docx);
                }
                catch (Exception e)
                {
                    error = "can't open File name \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                    File.AppendAllText("ReplaceXML_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }

                if (docx.IndexOf("ts") != -1)
                {
                    isTs = true;
                }

                if (docx.IndexOf("arabi") != -1 || docx.IndexOf("Arabi") != -1)
                {
                    isArabi = true;
                }
                else if (docx.IndexOf("zaban") != -1 || docx.IndexOf("Zaban") != -1)
                {
                    isZaban = true;
                }
                else
                {
                    isOther = true;
                }



                if (isTs)
                {
                   /* if (isZaban)
                    {
                        Result = Ts1En1.Replace(Result, ">1)");
                        Result = Ts2En1.Replace(Result, " 1)");

                        Result = Ts3En2.Replace(Result, ">2)");
                        Result = Ts4En2.Replace(Result, " 2)");

                        Result = Ts5En3.Replace(Result, ">3)");
                        Result = Ts6En3.Replace(Result, " 3)");

                        Result = Ts7En4.Replace(Result, ">4)");
                        Result = Ts8En4.Replace(Result, " 4)");
                    }
                    else
                    {
                        Result = Ts1Fa1.Replace(Result, ">1)");
                        Result = Ts2Fa1.Replace(Result, " 1)");

                        Result = Ts3Fa2.Replace(Result, ">2)");
                        Result = Ts4Fa2.Replace(Result, " 2)");

                        Result = Ts5Fa3.Replace(Result, ">3)");
                        Result = Ts6Fa3.Replace(Result, " 3)");
                        Result = Ts7Fa3.Replace(Result, ">3)");
                        Result = Ts8Fa3.Replace(Result, " 3)");

                        Result = Ts9Fa4.Replace(Result,  ">4)");
                        Result = Ts10Fa4.Replace(Result, " 4)");
                        Result = Ts11Fa4.Replace(Result, ">4)");
                        Result = Ts12Fa4.Replace(Result, " 4)");
                    }*/
                }

                Result = RgxRemoveUnsigned9.Replace(Result, "");
                Result = RgxRemoveUnsigned8.Replace(Result, "");
                Result = RgxRemoveUnsigned7.Replace(Result, "");
                Result = RgxRemoveUnsigned6.Replace(Result, "");
                Result = RgxRemoveUnsigned5.Replace(Result, "");
                Result = RgxRemoveUnsigned4.Replace(Result, "");
                Result = RgxRemoveUnsigned3.Replace(Result, "");
                Result = RgxRemoveUnsigned2.Replace(Result, "");
                Result = RgxRemoveUnsigned1.Replace(Result, "");

                Result = RgxRemove0.Replace(Result, "");

                Result = RgxRemove0O1.Replace(Result, "");

                Result = RgxRemove0O2.Replace(Result, "");

                Result = RgxRemove1.Replace(Result, "");

                Result = RgxReplace2.Replace(Result, "<w:r>\r\n<w:t xml:space=\"preserve\"> </w:t>\r\n</w:r>");

                if (isZaban)
                    Result = RgxReplace3.Replace(Result, "<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\" w:cs=\"Arial\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>");

                if (isArabi)
                    Result = RgxReplace3.Replace(Result, "<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rFonts w:cs=\"B Badr\"  w:hint=\"eastAsia\" />\r\n<w:b />\r\n<w:bCs />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"32\" />\r\n<w:szCs w:val=\"32\" />\r\n</w:rPr>");

                if (isOther)
                    Result = RgxReplace3.Replace(Result, "<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>");


                if (isZaban)
                {
                    Result = RgxReplace4O1JustZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr><w:t xml:space=\"preserve\">$1</w:t></w:r>");

                    Result = RgxReplace4JustZaban.Replace(Result, "<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr><w:t xml:space=\"preserve\">$1</w:t></w:r>");

                    Result = RgxReplace5O1JustZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr><w:t>$1</w:t></w:r>");

                    Result = RgxReplace5JustZaban.Replace(Result, "<w:rPr>\r\n<w:u w:val=\"single\" />\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr><w:t>$1</w:t></w:r>");

                    Result = RgxReplace6O1JustZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t xml:space=\"preserve\">$1</w:t>\r\n</w:r>");

                    Result = RgxReplace6JustZaban.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t xml:space=\"preserve\">$1</w:t>\r\n</w:r>");

                    Result = RgxReplace7O1JustZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t>$1</w:t>\r\n</w:r>");

                    Result = RgxReplace7JustZaban.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t>$1</w:t>\r\n</w:r>");
                }



                if (!isZaban)
                {
                    Result = RgxReplace8.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:t");
                    Result = RgxReplace9NotZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:rPr>");
                }


                if (isArabi)
                    Result = RgxReplace10.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rFonts w:cs=\"B Badr\"  w:hint=\"eastAsia\" />\r\n<w:b />\r\n<w:bCs />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"32\" />\r\n<w:szCs w:val=\"32\" />\r\n</w:rPr>\r\n<w:t ");

                if (isZaban)
                    Result = RgxReplace10.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\" w:cs=\"Arial\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t ");

                if (isOther)
                    Result = RgxReplace10.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t ");


                if (isArabi)
                    Result = RgxReplace11.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rFonts w:cs=\"B Badr\"  w:hint=\"eastAsia\" />\r\n<w:b />\r\n<w:bCs />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"32\" />\r\n<w:szCs w:val=\"32\" />\r\n</w:rPr>\r\n<w:t>");

                if (isZaban)
                    Result = RgxReplace11.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rFonts w:ascii=\"Arial\" w:hAnsi=\"Arial\" w:cs=\"Arial\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t>");

                if (isOther)
                    Result = RgxReplace11.Replace(Result, "<w:r>\r\n<w:rPr>\r\n<w:rtl />\r\n<w:b />\r\n<w:bCs />\r\n<w:rFonts w:cs=\"B Nazanin\"  w:hint=\"eastAsia\" />\r\n<w:rtl />\r\n<w:lang w:bidi=\"fa-IR\" />\r\n<w:sz w:val=\"26\" />\r\n<w:szCs w:val=\"26\" />\r\n</w:rPr>\r\n<w:t>");

                if (!isZaban)
                {
                    Result = RgxReplace12NotZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:object");
                    Result = RgxReplace13NotZaban.Replace(Result, "<w:p>\r\n<w:pPr>\r\n<w:bidi />\r\n</w:pPr>\r\n<w:r>\r\n<w:drawing");
                }

                Result = RgxReplace14.Replace(Result, "<w:sectPr>\r\n<w:pgSz w:w=\"12242\" w:h=\"26649\" />\r\n<w:pgMar w:top=\"284\" w:right=\"1440\" w:bottom=\"284\" w:left=\"1440\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\" />\r\n<w:cols w:space=\"720\" />\r\n<w:docGrid w:linePitch=\"360\" />\r\n</w:sectPr>");


                try
                {
                    File.WriteAllText(docx, Result);
                    Console.WriteLine(docx);
                }
                catch (Exception e)
                {
                    error = "can't save File name \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                    File.AppendAllText("ReplaceXML_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }


            }
        }
    }
}
