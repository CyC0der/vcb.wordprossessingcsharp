﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using Application1 = System.Windows.Forms.Application;
using Application2 = Microsoft.Office.Interop.Word.Application;

namespace DocxSideBySideReviewer
{
    public partial class Form1 : Form
    {

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
        static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern ushort TileWindows(IntPtr hwndParent, uint wHow, IntPtr lpRect, uint cKids, IntPtr lpKids);

        const int WM_COMMAND = 0x111;
        const int MIN_ALL = 419;
        const int MIN_ALL_UNDO = 416;


        Application2 app1;
        Application2 app2;
        Document doc1;
        Document doc2;


        public Form1()
        {
            InitializeComponent();

            openApp();


            //TileWindows(NULL, MDITILE_VERTICAL, NULL, 0, NULL);

            /* GCHandle gcHandle = GCHandle.Alloc(handles, GCHandleType.Pinned);

             IntPtr arrayHandle = gcHandle.AddrOfPinnedObject();
             TileWindows(IntPtr.Zero,
                         (uint)(Tile.Vertical | Tile.SkipDisabled),
                         IntPtr.Zero,
                         (uint)handles.Length, arrayHandle);

             gcHandle.Free();*/

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void openApp()
        {

            if (app1 == null)
            {
                app1 = new Application2();
                app1.Visible = false;
            }

            if (app2 == null)
            {
                app2 = new Application2();

                app2.Visible = false;

            }


           
        }

        private void close()
        {
            if (doc1 != null)
                try
                {
                    app1.ActiveWindow.Close();
                }
                catch (Exception)
                {

                }

            if (doc2 != null) try
                {
                    app2.ActiveWindow.Close();
                }
                catch (Exception)
                {

                }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openApp();

            close();

            app1.Visible = true;
            app2.Visible = true;

       


            /*IntPtr lHwnd = FindWindow("Shell_TrayWnd", null);
            SendMessage(lHwnd, WM_COMMAND, (IntPtr)MIN_ALL, IntPtr.Zero);
            System.Threading.Thread.Sleep(2000);
            SendMessage(lHwnd, WM_COMMAND, (IntPtr)MIN_ALL_UNDO, IntPtr.Zero);
            */

            doc1 = new Document();
            doc2 = new Document();

            string QPath = Path.ChangeExtension(textBox1.Text, "docx");
            string APath = Path.Combine(Path.GetDirectoryName(QPath) ,  Path.GetFileName(QPath).Replace("q","a"));



            if (!File.Exists(QPath))
            {
                MessageBox.Show("داش ممد شرمنده آدرس اشتباهه :/ ?");
                return;
            }

            try
            {
                doc1 = app1.Documents.Open(QPath);
            }
            catch (Exception er)
            {
                MessageBox.Show("\r\n داش ممد ببخشید فایل سوال خرابه باز نمیشه :(  \r\n" + er.Message);
                return;
            }


            if (File.Exists(APath))
            {
                try{
                    doc2 = app2.Documents.Open(APath);
                }
                catch (Exception er)
                {
                    MessageBox.Show("\r\n داش ممد ببخشید فایل پاسخنامه خرابه باز نمیشه :(  \r\n" + er.Message);
                }
            }

            doc1.Activate();

            app1.ActiveWindow.Left = 0;
            app1.ActiveWindow.Top = 0;
            app1.Height = SystemInformation.WorkingArea.Height * 72 / 96;
            app1.Width = (SystemInformation.WorkingArea.Width * 72 / 96) / 2;

            if (File.Exists(APath))
            {
                doc2.Activate();
                app2.ActiveWindow.Left = (SystemInformation.WorkingArea.Width * 72 / 96) / 2;
                app2.ActiveWindow.Top = 0;
                app2.Height = SystemInformation.WorkingArea.Height * 72 / 96;
                app2.Width = (SystemInformation.WorkingArea.Width * 72 / 96) / 2;

            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            close();
        }
    }
}
