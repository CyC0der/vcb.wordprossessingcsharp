﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Ionic.Zip;

namespace MsWordBatchTools
{
    class pack
    {
        private Form1 form;
        private bool chBPackRemove;
        public bool radBPackAll;

        public pack(Form1 form,bool radBPackAll,bool chBPackRemove)
        {
            this.form = form;
            this.chBPackRemove = chBPackRemove;
            this.radBPackAll = radBPackAll;
        }

        public void zipALL(FileInfo docx)
        {
            string sourceFolder = docx.FullName.Replace(".docx", "");

            if (!Directory.Exists(sourceFolder))
            {
                form.ErLogWrite(" ** can't find unpacked directory \r\nFile:\r\n"+docx.Name+"\r\n\r\n ");
                return;
            }

            long dirSize = Form1.DirSize(new DirectoryInfo(sourceFolder));

            /* امکان ندارد اندازه یک فولد فایل ورد آنکپ شده از 20 کیلوبایت کمتر باشد این راهی است بهتر از هیچی برای چک کردن فولدر ورد*/
            if (dirSize == 0 || dirSize < 25480)
            {
                form.ErLogWrite(" ** invalid unpacked directory \r\nDirectory:\r\n" + sourceFolder + "\r\n\r\n ");
                return;
            }

            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    try
                    {
                        zip.AddDirectory(sourceFolder);
                    }
                    catch (Exception er)
                    {
                        form.ErLogWrite(" - adding files to pack failed!\r\nDirectory:" + sourceFolder + "\r\nExeption: " + er.Message + "\r\n\r\n");
                    }

                    try
                    {
                        zip.Save(docx.FullName);
                        form.logWrite(" - docx packed successfuly\r\n");

                        if (chBPackRemove)
                            removeDir(sourceFolder);
                    }
                    catch (Exception er3)
                    {
                        form.ErLogWrite(" - can't save pack file\r\nDirectory:" + sourceFolder + "\r\nExeption: " + er3.Message + "\r\n\r\n");
                    }
                }
            }
            catch (Exception e)
            {
               form.ErLogWrite(" - can't create zip file !\r\nFolder:" + sourceFolder + "\r\nExeption: " + e.Message + "\r\n\r\n");
            }
        }

        public void turnBackDocumentXML(FileInfo docx)
        {
            string sourceFile = docx.FullName.Replace(".docx", ".xml");

            if (!File.Exists(sourceFile))
            {
                form.ErLogWrite(" ** can't find unpacked xml file \r\nFile:\r\n" + sourceFile + "\r\n\r\n ");
                return;
            }

            FileInfo XmlFile = new FileInfo(sourceFile);

 
            if (XmlFile.Length < 1000)
            {
                form.ErLogWrite(" ** invalid unpacked xml file \r\nFile:\r\n" + sourceFile + "\r\n\r\n ");
                XmlFile = null;
                return;
            }
            XmlFile = null;


            unpack UnPacker = new unpack(form,true,false);
            UnPacker.unzipALL(docx);
            UnPacker = null;

            try
            {
                form.logWrite(" - trying copy XML file to /word/document.xml \r\n");
                File.Copy(sourceFile, docx.FullName.Replace(".docx", "/word/document.xml"), true);
            }
            catch(Exception er)
            {
                form.ErLogWrite(" - can't copy XML file \r\n File: \r\n" + sourceFile + "\r\nExeption:" + er.Message);
            }

 
            zipALL(docx);

            if (chBPackRemove)
                try
                {
                    File.Delete(sourceFile);
                }
                catch (Exception er)
                {
                    form.ErLogWrite(" - can't Delete XML file \r\n File: \r\n" + sourceFile + "\r\nExeption:" + er.Message);
                }
        }

        public void removeDir(string dirName)
        {
            form.logWrite(" - Tying to remove unpacked directory! (its may exists before)\r\n");
            System.IO.DirectoryInfo di = new DirectoryInfo(dirName);

            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception ex)
                {
                    form.ErLogWrite(" *** can't delete a file in directory\r\nFile:"+file.Name+"\r\nException:"+ex.Message+"\r\n");
                }

            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete(true);
                }
                catch (Exception ex)
                {
                    form.ErLogWrite(" *** can't delete a directory\r\nException:" + ex.Message + "\r\n");
                }

            }
        }
    }
}
