﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Threading;
using Task2 = System.Threading.Tasks.Task;
using System.Drawing;
using System.IO;

namespace MsWordBatchTools
{
    class  DocxToPNG
    {
        private bool WordWinVisible;
        private decimal PNGWidth;
        private decimal PNGHeight;

        private Application app;
        private Document doc;

        private Form1 form;

        public DocxToPNG(Form1 form,bool WordWinVisible, decimal PNGWidth, decimal PNGHeight)
        {
            this.form = form;
            this.WordWinVisible = WordWinVisible;
            this.PNGWidth = PNGWidth;
            this.PNGHeight = PNGHeight;
        }


      /*  public void test()
        {
 

        }*/

        public void convert(FileInfo docx)
        {
            string destFolder = docx.FullName.Replace(".docx", "_png");

            if (!Directory.Exists(destFolder))
            {
                try
                {
                    Directory.CreateDirectory(destFolder);
                    form.logWrite(" - *_png directory Created.\r\n");
                }
                catch (Exception e)
                {
                    form.ErLogWrite(" *** can't create *_png directory\r\nPath:"+destFolder+"\r\nException:"+e.Message+"\r\n");
                    return;
                }
            }
            else
            {
                form.logWrite(" - *_png directory already exists.\r\n");
            }

            try
            {
                
                doc = new Document();

                //doc.Application.WindowActivate += test;

                doc = app.Documents.Open(docx.FullName);
                form.logWrite(" - docx file "+docx.Name+" opened.\r\n");
            }
            catch (Exception er)
            {
                form.ErLogWrite(" *** can't open docx file\r\nFile:" + docx.FullName + "\r\nException:" + er.Message + "\r\n");
            }


            //form.logWrite(doc.ActiveWindow.Caption);
            //hread.Sleep(1000);
            //                doc.Application.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;
            

            doc.ShowGrammaticalErrors = false;
            doc.ShowRevisions = false;
            doc.ShowSpellingErrors = false;
            
             form.logWrite(" - start looping pages...\r\n");
            //Opens the word document and fetch each page and converts to image
            foreach (Microsoft.Office.Interop.Word.Window window in doc.Windows)
            {
                foreach (Microsoft.Office.Interop.Word.Pane pane in window.Panes)
                {
                    for (var i = 1; i <= pane.Pages.Count; i++)
                    {
                        form.logWrite(" - fetching page("+i.ToString()+") data\r\n");
                        Microsoft.Office.Interop.Word.Page page = null;
                        bool populated = false;
                        while (!populated)
                        {
                            try
                            {
                                // This !@#$ variable won't always be ready to spill its pages. If you step through
                                // the code, it will always work.  If you just execute it, it will crash.  So what
                                // I am doing is letting the code catch up a little by letting the thread sleep
                                // for a microsecond.  The second time around, this variable should populate ok.
                                page = pane.Pages[i];
                                populated = true;
                            }
                            catch (COMException ex)
                            {
                                Thread.Sleep(1);
                            }
                        }
                        var bits = page.EnhMetaFileBits;
                        var pngTarget = destFolder + "\\" + i.ToString() + ".png";

                        try
                        {
                            form.logWrite(" - getting from MemmoryStream page("+i.ToString()+")\r\n");
                            using (var ms = new MemoryStream((byte[])(bits)))
                            {
                                Image image = Image.FromStream(ms);
                                Bitmap myBitmap = new Bitmap(image, new Size(Convert.ToInt32(PNGWidth), Convert.ToInt32(PNGHeight)));
                                myBitmap.Save(pngTarget, System.Drawing.Imaging.ImageFormat.Png);
                                form.logWrite(" - PNG saved.\r\n");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            form.ErLogWrite(" *** can't save PNG page("+i.ToString()+")\r\nFile:" + docx.FullName + "\r\nException:" + ex.Message + "\r\n");
                            doc.Close(false, Type.Missing, Type.Missing);
                            Marshal.ReleaseComObject(doc);
                            doc = null;

                        }
                    }
                }
            }

            doc.Close(false, Type.Missing, Type.Missing);

 
            Marshal.ReleaseComObject(doc);
  
            form.logWrite(" - docx closed.\r\n\r\n");
        }

        public void init()
        {
            app = new Application();

            if (WordWinVisible)
                app.Visible = true;
            else
                app.Visible = false;
        }

        public void finished()
        {
            if (app != null)
            {
                app.Quit(false, Type.Missing, Type.Missing);
                Marshal.ReleaseComObject(app);
                app = null;
            }
        }
    }
}
