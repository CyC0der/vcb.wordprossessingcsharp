﻿namespace MsWordBatchTools
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LBUnPackCount = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.btnUnPack = new System.Windows.Forms.Button();
            this.chBUnPackExistsAlert = new System.Windows.Forms.CheckBox();
            this.radBUnPackAll = new System.Windows.Forms.RadioButton();
            this.radBUnpackDocument = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.LBDocxToPNGCounter = new System.Windows.Forms.Label();
            this.txtPNGDelay = new System.Windows.Forms.NumericUpDown();
            this.txtPNGHeight = new System.Windows.Forms.NumericUpDown();
            this.txtPNGWidth = new System.Windows.Forms.NumericUpDown();
            this.chBPNGVisibleWord = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnConvertPNG = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.SelectPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.LBPackCounter = new System.Windows.Forms.Label();
            this.chBPackRemove = new System.Windows.Forms.CheckBox();
            this.radBPackAll = new System.Windows.Forms.RadioButton();
            this.radBPackDocument = new System.Windows.Forms.RadioButton();
            this.btnPack = new System.Windows.Forms.Button();
            this.btnSTOP = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LBWMFCounter = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnWMFConvert = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtWMFFileExt = new System.Windows.Forms.TextBox();
            this.radBWMFToPNG = new System.Windows.Forms.RadioButton();
            this.radBWMFToMathML = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtWMFConvertTo = new System.Windows.Forms.TextBox();
            this.LBDocx = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.LBprecentShow = new System.Windows.Forms.Label();
            this.reset = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.errorLog = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGWidth)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LBUnPackCount);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.btnUnPack);
            this.groupBox1.Controls.Add(this.chBUnPackExistsAlert);
            this.groupBox1.Controls.Add(this.radBUnPackAll);
            this.groupBox1.Controls.Add(this.radBUnpackDocument);
            this.groupBox1.Location = new System.Drawing.Point(518, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 170);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "UnPack Docx";
            // 
            // LBUnPackCount
            // 
            this.LBUnPackCount.AutoSize = true;
            this.LBUnPackCount.Location = new System.Drawing.Point(93, 143);
            this.LBUnPackCount.Name = "LBUnPackCount";
            this.LBUnPackCount.Size = new System.Drawing.Size(13, 13);
            this.LBUnPackCount.TabIndex = 5;
            this.LBUnPackCount.Text = "0";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(15, 143);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(79, 13);
            this.label45.TabIndex = 4;
            this.label45.Text = "unpacked files:";
            // 
            // btnUnPack
            // 
            this.btnUnPack.Enabled = false;
            this.btnUnPack.Location = new System.Drawing.Point(15, 104);
            this.btnUnPack.Name = "btnUnPack";
            this.btnUnPack.Size = new System.Drawing.Size(170, 28);
            this.btnUnPack.TabIndex = 3;
            this.btnUnPack.Text = "UnPack";
            this.btnUnPack.UseVisualStyleBackColor = true;
            this.btnUnPack.Click += new System.EventHandler(this.btnUnPack_Click);
            // 
            // chBUnPackExistsAlert
            // 
            this.chBUnPackExistsAlert.AutoSize = true;
            this.chBUnPackExistsAlert.Location = new System.Drawing.Point(17, 79);
            this.chBUnPackExistsAlert.Name = "chBUnPackExistsAlert";
            this.chBUnPackExistsAlert.Size = new System.Drawing.Size(154, 17);
            this.chBUnPackExistsAlert.TabIndex = 2;
            this.chBUnPackExistsAlert.Text = "Alert me if file already exists";
            this.chBUnPackExistsAlert.UseVisualStyleBackColor = true;
            // 
            // radBUnPackAll
            // 
            this.radBUnPackAll.AutoSize = true;
            this.radBUnPackAll.Location = new System.Drawing.Point(17, 45);
            this.radBUnPackAll.Name = "radBUnPackAll";
            this.radBUnPackAll.Size = new System.Drawing.Size(89, 17);
            this.radBUnPackAll.TabIndex = 1;
            this.radBUnPackAll.TabStop = true;
            this.radBUnPackAll.Text = "Extact all files";
            this.radBUnPackAll.UseVisualStyleBackColor = true;
            // 
            // radBUnpackDocument
            // 
            this.radBUnpackDocument.AutoSize = true;
            this.radBUnpackDocument.Checked = true;
            this.radBUnpackDocument.Location = new System.Drawing.Point(17, 21);
            this.radBUnpackDocument.Name = "radBUnpackDocument";
            this.radBUnpackDocument.Size = new System.Drawing.Size(148, 17);
            this.radBUnpackDocument.TabIndex = 0;
            this.radBUnpackDocument.TabStop = true;
            this.radBUnpackDocument.Text = "Extract document.xml only";
            this.radBUnpackDocument.UseVisualStyleBackColor = true;
            this.radBUnpackDocument.CheckedChanged += new System.EventHandler(this.radBUnpackDocument_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.LBDocxToPNGCounter);
            this.groupBox2.Controls.Add(this.txtPNGDelay);
            this.groupBox2.Controls.Add(this.txtPNGHeight);
            this.groupBox2.Controls.Add(this.txtPNGWidth);
            this.groupBox2.Controls.Add(this.chBPNGVisibleWord);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnConvertPNG);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(725, 53);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(181, 272);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Docx To PNG";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(20, 244);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(79, 13);
            this.label41.TabIndex = 12;
            this.label41.Text = "converted files:";
            // 
            // LBDocxToPNGCounter
            // 
            this.LBDocxToPNGCounter.AutoSize = true;
            this.LBDocxToPNGCounter.Location = new System.Drawing.Point(97, 245);
            this.LBDocxToPNGCounter.Name = "LBDocxToPNGCounter";
            this.LBDocxToPNGCounter.Size = new System.Drawing.Size(13, 13);
            this.LBDocxToPNGCounter.TabIndex = 11;
            this.LBDocxToPNGCounter.Text = "0";
            // 
            // txtPNGDelay
            // 
            this.txtPNGDelay.Location = new System.Drawing.Point(19, 135);
            this.txtPNGDelay.Name = "txtPNGDelay";
            this.txtPNGDelay.Size = new System.Drawing.Size(101, 20);
            this.txtPNGDelay.TabIndex = 10;
            // 
            // txtPNGHeight
            // 
            this.txtPNGHeight.Location = new System.Drawing.Point(18, 91);
            this.txtPNGHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtPNGHeight.Name = "txtPNGHeight";
            this.txtPNGHeight.Size = new System.Drawing.Size(102, 20);
            this.txtPNGHeight.TabIndex = 9;
            this.txtPNGHeight.Value = new decimal(new int[] {
            5551,
            0,
            0,
            0});
            // 
            // txtPNGWidth
            // 
            this.txtPNGWidth.Location = new System.Drawing.Point(19, 46);
            this.txtPNGWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.txtPNGWidth.Name = "txtPNGWidth";
            this.txtPNGWidth.Size = new System.Drawing.Size(101, 20);
            this.txtPNGWidth.TabIndex = 8;
            this.txtPNGWidth.Value = new decimal(new int[] {
            2550,
            0,
            0,
            0});
            // 
            // chBPNGVisibleWord
            // 
            this.chBPNGVisibleWord.AutoSize = true;
            this.chBPNGVisibleWord.Location = new System.Drawing.Point(20, 181);
            this.chBPNGVisibleWord.Name = "chBPNGVisibleWord";
            this.chBPNGVisibleWord.Size = new System.Drawing.Size(121, 17);
            this.chBPNGVisibleWord.TabIndex = 7;
            this.chBPNGVisibleWord.Text = "Visible word window";
            this.chBPNGVisibleWord.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Delay ( Millisecond ):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Height:";
            // 
            // btnConvertPNG
            // 
            this.btnConvertPNG.Enabled = false;
            this.btnConvertPNG.Location = new System.Drawing.Point(18, 205);
            this.btnConvertPNG.Name = "btnConvertPNG";
            this.btnConvertPNG.Size = new System.Drawing.Size(147, 29);
            this.btnConvertPNG.TabIndex = 4;
            this.btnConvertPNG.Text = "Convert to PNG";
            this.btnConvertPNG.UseVisualStyleBackColor = true;
            this.btnConvertPNG.Click += new System.EventHandler(this.btnConvertPNG_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Width:";
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(12, 25);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(500, 336);
            this.txtLog.TabIndex = 2;
            this.txtLog.Text = "";
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // SelectPath
            // 
            this.SelectPath.Location = new System.Drawing.Point(831, 24);
            this.SelectPath.Name = "SelectPath";
            this.SelectPath.Size = new System.Drawing.Size(75, 23);
            this.SelectPath.TabIndex = 3;
            this.SelectPath.Text = "Select...";
            this.SelectPath.UseVisualStyleBackColor = true;
            this.SelectPath.Click += new System.EventHandler(this.SelectPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Log View:";
            // 
            // folderPath
            // 
            this.folderPath.Location = new System.Drawing.Point(518, 25);
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(307, 20);
            this.folderPath.TabIndex = 5;
            this.folderPath.TextChanged += new System.EventHandler(this.folderPath_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(516, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Docx\'s Folder Location:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.LBPackCounter);
            this.groupBox3.Controls.Add(this.chBPackRemove);
            this.groupBox3.Controls.Add(this.radBPackAll);
            this.groupBox3.Controls.Add(this.radBPackDocument);
            this.groupBox3.Controls.Add(this.btnPack);
            this.groupBox3.Location = new System.Drawing.Point(517, 229);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 165);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pack Docx";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(17, 141);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(67, 13);
            this.label40.TabIndex = 5;
            this.label40.Text = "packed files:";
            // 
            // LBPackCounter
            // 
            this.LBPackCounter.AutoSize = true;
            this.LBPackCounter.Location = new System.Drawing.Point(84, 141);
            this.LBPackCounter.Name = "LBPackCounter";
            this.LBPackCounter.Size = new System.Drawing.Size(13, 13);
            this.LBPackCounter.TabIndex = 4;
            this.LBPackCounter.Text = "0";
            // 
            // chBPackRemove
            // 
            this.chBPackRemove.AutoSize = true;
            this.chBPackRemove.Location = new System.Drawing.Point(13, 79);
            this.chBPackRemove.Name = "chBPackRemove";
            this.chBPackRemove.Size = new System.Drawing.Size(138, 17);
            this.chBPackRemove.TabIndex = 3;
            this.chBPackRemove.Text = "Remove unpacked files";
            this.chBPackRemove.UseVisualStyleBackColor = true;
            // 
            // radBPackAll
            // 
            this.radBPackAll.AutoSize = true;
            this.radBPackAll.Location = new System.Drawing.Point(13, 45);
            this.radBPackAll.Name = "radBPackAll";
            this.radBPackAll.Size = new System.Drawing.Size(84, 17);
            this.radBPackAll.TabIndex = 2;
            this.radBPackAll.TabStop = true;
            this.radBPackAll.Text = "Pack all files";
            this.radBPackAll.UseVisualStyleBackColor = true;
            // 
            // radBPackDocument
            // 
            this.radBPackDocument.AutoSize = true;
            this.radBPackDocument.Checked = true;
            this.radBPackDocument.Location = new System.Drawing.Point(13, 21);
            this.radBPackDocument.Name = "radBPackDocument";
            this.radBPackDocument.Size = new System.Drawing.Size(164, 17);
            this.radBPackDocument.TabIndex = 1;
            this.radBPackDocument.TabStop = true;
            this.radBPackDocument.Text = "Turn back document.xml only";
            this.radBPackDocument.UseVisualStyleBackColor = true;
            // 
            // btnPack
            // 
            this.btnPack.Enabled = false;
            this.btnPack.Location = new System.Drawing.Point(13, 103);
            this.btnPack.Name = "btnPack";
            this.btnPack.Size = new System.Drawing.Size(172, 29);
            this.btnPack.TabIndex = 0;
            this.btnPack.Text = "Pack";
            this.btnPack.UseVisualStyleBackColor = true;
            this.btnPack.Click += new System.EventHandler(this.btnPack_Click);
            // 
            // btnSTOP
            // 
            this.btnSTOP.Enabled = false;
            this.btnSTOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSTOP.Location = new System.Drawing.Point(725, 332);
            this.btnSTOP.Name = "btnSTOP";
            this.btnSTOP.Size = new System.Drawing.Size(181, 62);
            this.btnSTOP.TabIndex = 8;
            this.btnSTOP.Text = "STOP";
            this.btnSTOP.UseVisualStyleBackColor = true;
            this.btnSTOP.Click += new System.EventHandler(this.btnSTOP_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(920, 29);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(49, 13);
            this.label43.TabIndex = 11;
            this.label43.Text = "docx file:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LBWMFCounter);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.btnWMFConvert);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.txtWMFFileExt);
            this.groupBox4.Controls.Add(this.radBWMFToPNG);
            this.groupBox4.Controls.Add(this.radBWMFToMathML);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtWMFConvertTo);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(914, 53);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(145, 272);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "WMF Converter";
            // 
            // LBWMFCounter
            // 
            this.LBWMFCounter.AutoSize = true;
            this.LBWMFCounter.Location = new System.Drawing.Point(96, 245);
            this.LBWMFCounter.Name = "LBWMFCounter";
            this.LBWMFCounter.Size = new System.Drawing.Size(13, 13);
            this.LBWMFCounter.TabIndex = 8;
            this.LBWMFCounter.Text = "0";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(17, 245);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(80, 13);
            this.label42.TabIndex = 7;
            this.label42.Text = "Converted files:";
            // 
            // btnWMFConvert
            // 
            this.btnWMFConvert.Enabled = false;
            this.btnWMFConvert.Location = new System.Drawing.Point(13, 205);
            this.btnWMFConvert.Name = "btnWMFConvert";
            this.btnWMFConvert.Size = new System.Drawing.Size(119, 29);
            this.btnWMFConvert.TabIndex = 6;
            this.btnWMFConvert.Text = "Convert";
            this.btnWMFConvert.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "File Extention:";
            // 
            // txtWMFFileExt
            // 
            this.txtWMFFileExt.Location = new System.Drawing.Point(13, 104);
            this.txtWMFFileExt.Name = "txtWMFFileExt";
            this.txtWMFFileExt.Size = new System.Drawing.Size(120, 20);
            this.txtWMFFileExt.TabIndex = 4;
            this.txtWMFFileExt.Text = "mathml";
            // 
            // radBWMFToPNG
            // 
            this.radBWMFToPNG.AutoSize = true;
            this.radBWMFToPNG.Location = new System.Drawing.Point(14, 52);
            this.radBWMFToPNG.Name = "radBWMFToPNG";
            this.radBWMFToPNG.Size = new System.Drawing.Size(104, 17);
            this.radBWMFToPNG.TabIndex = 3;
            this.radBWMFToPNG.Text = "Convert To PNG";
            this.radBWMFToPNG.UseVisualStyleBackColor = true;
            // 
            // radBWMFToMathML
            // 
            this.radBWMFToMathML.AutoSize = true;
            this.radBWMFToMathML.Checked = true;
            this.radBWMFToMathML.Location = new System.Drawing.Point(14, 29);
            this.radBWMFToMathML.Name = "radBWMFToMathML";
            this.radBWMFToMathML.Size = new System.Drawing.Size(120, 17);
            this.radBWMFToMathML.TabIndex = 2;
            this.radBWMFToMathML.TabStop = true;
            this.radBWMFToMathML.Text = "Convert To MathML";
            this.radBWMFToMathML.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 137);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Convert To:";
            // 
            // txtWMFConvertTo
            // 
            this.txtWMFConvertTo.Location = new System.Drawing.Point(13, 153);
            this.txtWMFConvertTo.Name = "txtWMFConvertTo";
            this.txtWMFConvertTo.Size = new System.Drawing.Size(120, 20);
            this.txtWMFConvertTo.TabIndex = 0;
            this.txtWMFConvertTo.Text = "MathML2 (m namespace).tdl";
            // 
            // LBDocx
            // 
            this.LBDocx.AutoSize = true;
            this.LBDocx.Location = new System.Drawing.Point(975, 29);
            this.LBDocx.Name = "LBDocx";
            this.LBDocx.Size = new System.Drawing.Size(13, 13);
            this.LBDocx.TabIndex = 23;
            this.LBDocx.Text = "0";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(48, 371);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(463, 23);
            this.progressBar1.TabIndex = 25;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // LBprecentShow
            // 
            this.LBprecentShow.AutoSize = true;
            this.LBprecentShow.BackColor = System.Drawing.Color.Transparent;
            this.LBprecentShow.Location = new System.Drawing.Point(10, 376);
            this.LBprecentShow.Name = "LBprecentShow";
            this.LBprecentShow.Size = new System.Drawing.Size(21, 13);
            this.LBprecentShow.TabIndex = 26;
            this.LBprecentShow.Text = "0%";
            // 
            // reset
            // 
            this.reset.Enabled = false;
            this.reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.reset.Location = new System.Drawing.Point(914, 332);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(145, 62);
            this.reset.TabIndex = 27;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(920, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Errors:";
            // 
            // errorLog
            // 
            this.errorLog.AutoSize = true;
            this.errorLog.Location = new System.Drawing.Point(975, 9);
            this.errorLog.Name = "errorLog";
            this.errorLog.Size = new System.Drawing.Size(13, 13);
            this.errorLog.TabIndex = 29;
            this.errorLog.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 403);
            this.Controls.Add(this.errorLog);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.reset);
            this.Controls.Add(this.LBprecentShow);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.LBDocx);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.btnSTOP);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.folderPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SelectPath);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Word Tools , Unpack , Pack , to PNG , to WMF  By Kourosh Raoufi";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPNGWidth)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Button SelectPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox folderPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUnPack;
        private System.Windows.Forms.CheckBox chBUnPackExistsAlert;
        private System.Windows.Forms.RadioButton radBUnPackAll;
        private System.Windows.Forms.RadioButton radBUnpackDocument;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radBPackAll;
        private System.Windows.Forms.RadioButton radBPackDocument;
        private System.Windows.Forms.Button btnPack;
        private System.Windows.Forms.CheckBox chBPackRemove;
        private System.Windows.Forms.Button btnConvertPNG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtPNGDelay;
        private System.Windows.Forms.NumericUpDown txtPNGHeight;
        private System.Windows.Forms.NumericUpDown txtPNGWidth;
        private System.Windows.Forms.CheckBox chBPNGVisibleWord;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSTOP;
        private System.Windows.Forms.Label LBUnPackCount;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnWMFConvert;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtWMFFileExt;
        private System.Windows.Forms.RadioButton radBWMFToPNG;
        private System.Windows.Forms.RadioButton radBWMFToMathML;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtWMFConvertTo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label LBDocxToPNGCounter;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label LBPackCounter;
        private System.Windows.Forms.Label LBWMFCounter;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label LBDocx;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label LBprecentShow;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label errorLog;
    }
}

