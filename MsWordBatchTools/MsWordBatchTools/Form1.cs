﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace MsWordBatchTools
{
    public partial class Form1 : Form
    {
        const int DOWORK_CONVERTPNG = 1;
        const int DOWORK_PACK = 2;
        const int DOWORK_UNPACK = 3;
        const int DOWORK_CONVERTWMF = 4;

        private unpack up;
        private pack p;
        private DocxToPNG dtp;
        private int DoWorkType;
        public int ErrorCounter = 0;

        public string[] DocxList;
        public int DocxListPointer = 0;

        public Form1()
        {
            InitializeComponent();

            if (File.Exists("wordTools_errors.log"))
                File.Delete("wordTools_errors.log");


            /*  About background Worker:
             *  این امکان را به شما می دهد که کدی را داخل ترید دیگر اجرا کنید ، کدهایی که دارای
             * ساختار حلقه ای هستند باعث فریز شدن و هنگ کردن فرم می شوند  اجرای کد در یک ترید جدید
             * این مشکل را حل میکند */

            /* فعال کردن این دو فلگ باعث میشود ترید ما مارا از پیش روی پروسس مطلع سازد*/
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            /* Binding events to the  background worker */
            /* code to run */
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            /* invloked when proccess end */
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            /* invoked when worker.ReportProgress() used in  doWork */
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;

            /* این فایل حاوی آخرین آدرس فولد انتخاب شده به وسیله کاربر است */
            if (File.Exists("2conf.ini"))
                folderPath.Text = System.IO.File.ReadAllText("2conf.ini");

        }

        public void resetDocxList()
        {
            setLabelToZero();
            DisableBottoms();
            DocxList = null;
            DocxListPointer = 0;
        }

        /*  این متد صحت آدرس انتخاب شده و اینکه آیا فایل ورد در آن است یا نه را چک کرده و لیست فایل های ورد را داخل یک آرایه رشته میریزد*/
        public bool fetchDocxList()
        {

            string[] list;

            resetDocxList();

            if (!Directory.Exists(folderPath.Text))
            {
                logWrite("** selected directory not exists \r\n\r\n");
                DisableBottoms();
                return false;
            }

            try
            {
                list = System.IO.Directory.GetFiles(folderPath.Text, "*.docx", System.IO.SearchOption.AllDirectories);
            }
            catch (Exception er)
            {
                logWrite("** can't access to selected directory! \r\n\r\n");
                DisableBottoms();
                return false;
            }


            LBDocx.Text = list.Length.ToString();

            if (list.Length != 0)
            {
                System.IO.File.WriteAllText("2conf.ini", folderPath.Text);

                this.DocxList = list;

                EnableBottoms();

                return true;
            }

           DisableBottoms();
            return false;
        }

        public static long DirSize(DirectoryInfo d)
        {
            long Size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                Size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                Size += DirSize(di);
            }
            return (Size);
        }

        /* این تابع در سر تا سر برنامه اجرا میشود و کارش نمایش گزارش کار در فرم است*/
        public void logWrite(string log){
 
            /* دلیل استفاده از این متد این است که دسترسی و وارد کردن مقدار از داخل توابع ورکر به کنتزل های فرم به شکل معمولی ممکن نیست*/  
            this.Invoke(new MethodInvoker(delegate
            {
                // Execute the following code on the GUI thread.
                txtLog.Text += log;
            }));
        }

        public void ErLogWrite(string log)
        {
            logWrite(log);

            using (StreamWriter sw = File.AppendText("wordTools_errors.log")) 
            {
                sw.WriteLine(log);
            }

            ErrorCounter++;

            this.Invoke(new MethodInvoker(delegate
            {
                errorLog.Text = ErrorCounter.ToString();
            }));
        }

        public void setLabelToZero()
        {
            LBDocx.Text = "0";
            LBPackCounter.Text = "0";
            LBWMFCounter.Text = "0";
            LBUnPackCount.Text = "0";
            LBDocxToPNGCounter.Text = "0";
        }

        public void DisableBottoms()
        {
            btnUnPack.Enabled = false;
            btnPack.Enabled = false;
            btnConvertPNG.Enabled = false;
            btnWMFConvert.Enabled = false;
            SelectPath.Enabled = false;
            reset.Enabled = false;
        }

        public void EnableBottoms()
        {
            btnUnPack.Enabled = true;
            btnPack.Enabled = true;
            btnConvertPNG.Enabled = true;
            btnWMFConvert.Enabled = true;
            SelectPath.Enabled = true;
            btnSTOP.Enabled = false;
        }

        private void radBUnpackDocument_CheckedChanged(object sender, EventArgs e)
        {
             if (radBUnpackDocument.Checked)
                MessageBox.Show("document.xml will rename to docx file name  like  q1.xml");
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {
            /* باعث میشود که اسکرول تکست باکس نمایش گزارش همیشه پایین بیاید */
            txtLog.SelectionStart = txtLog.Text.Length; //Set the current caret position at the end
            txtLog.ScrollToCaret(); //Now scroll it automatically
        }

        private void folderPath_TextChanged(object sender, EventArgs e)
        {
            resetDocxList();

            if (!Directory.Exists(folderPath.Text))
                return;

             fetchDocxList();
        }

        private void SelectPath_Click(object sender, EventArgs e)
        {
            /* if user click on OK then */
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath.Text = folderBrowserDialog1.SelectedPath;
                 resetDocxList();
            }
        }

        private void btnUnPack_Click(object sender, EventArgs e)
        {
            /* check if bwk is not busy */
            if (backgroundWorker1.IsBusy != true)
            {
                DoWorkType = DOWORK_UNPACK;

                btnSTOP.Enabled = true;

                DisableBottoms();

                this.logWrite("\r\n\t **** Start UnPacking ! ****\r\n\r\n");

                up = new unpack(this,radBUnPackAll.Checked,chBUnPackExistsAlert.Checked);

                backgroundWorker1.RunWorkerAsync();
            };
        }

        private void btnSTOP_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                backgroundWorker1.CancelAsync();
                EnableBottoms();
            }

            reset.Enabled = true;
        }

        private void btnPack_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                DoWorkType = DOWORK_PACK;

                btnSTOP.Enabled = true;

                DisableBottoms();

                this.logWrite("\r\n\t **** Start Packing ! ****\r\n\r\n");

                p = new pack(this, radBPackAll.Checked, chBPackRemove.Checked);

                backgroundWorker1.RunWorkerAsync();
            };
        }

        private void btnConvertPNG_Click(object sender, EventArgs e)
        {
           if (backgroundWorker1.IsBusy != true)
           {
               DoWorkType = DOWORK_CONVERTPNG;

               btnSTOP.Enabled = true;

               DisableBottoms();
              
               this.logWrite("\r\n\t **** Start Converting ! ****\r\n\r\n");

               dtp = new DocxToPNG(
                    this,
                   chBPNGVisibleWord.Checked,
                   txtPNGWidth.Value,
                   txtPNGHeight.Value
               );

               dtp.init();

                backgroundWorker1.RunWorkerAsync();
           }
        }

        private void BgW_ConvertPNG_DoWork(BackgroundWorker wk, DoWorkEventArgs e)
        {
            int dLen = DocxList.Length;

  
            for (; DocxListPointer < dLen; DocxListPointer++)
            {

                if (wk.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                FileInfo docx = new FileInfo(DocxList[DocxListPointer]);

                if ((docx.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    ErLogWrite(" ** Can't Open ReadOnly File \r\nFile:" + docx.FullName + "\r\n");
                    return;
                }

                dtp.convert(docx);

                if (txtPNGDelay.Value != 0)
                    Thread.Sleep(Convert.ToInt32(txtPNGDelay.Value));

                wk.ReportProgress(Convert.ToInt32(Math.Round(DocxListPointer * 100 / (decimal)dLen)));
            }
        }

        private void BgW_UnPack_DoWork(BackgroundWorker wk, DoWorkEventArgs e)
        {
            int dLen = DocxList.Length;

            for (; DocxListPointer < dLen; DocxListPointer++)
            {
                if (wk.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                FileInfo docx = new FileInfo(DocxList[DocxListPointer]);

                if (up.radBUnPackAll)
                {
                    up.unzipALL(docx);
                }
                else
                {
                    up.unZipDocumentXML(docx);
                }

                wk.ReportProgress(Convert.ToInt32(Math.Round(DocxListPointer * 100 / (decimal)dLen)));
            }
        }

        private void BgW_pack_DoWork(BackgroundWorker wk, DoWorkEventArgs e)
        {
            int dLen = DocxList.Length;

            for (; DocxListPointer < dLen; DocxListPointer++)
            {
                if (wk.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                FileInfo docx = new FileInfo(DocxList[DocxListPointer]);

                if (p.radBPackAll)
                {
                    p.zipALL(docx);
                }
                else
                {
                    p.turnBackDocumentXML(docx);
                }

                wk.ReportProgress(Convert.ToInt32(Math.Round(DocxListPointer * 100 / (decimal)dLen)));
            }
        }
       
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            switch (DoWorkType)
            {
                case DOWORK_CONVERTPNG:
                    BgW_ConvertPNG_DoWork(worker,e);
                break;
                case  DOWORK_PACK:
                    BgW_pack_DoWork(worker, e);
                break;
                case  DOWORK_UNPACK:
                    BgW_UnPack_DoWork(worker, e);
                break;
                case DOWORK_CONVERTWMF:
                break;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (DoWorkType)
            {
                case DOWORK_CONVERTPNG:
                    LBDocxToPNGCounter.Text = (DocxListPointer + 1).ToString();
                    break;
                case DOWORK_PACK:
                    LBPackCounter.Text = (DocxListPointer + 1).ToString();
                    break;
                case DOWORK_UNPACK:
                    LBUnPackCount.Text = (DocxListPointer + 1).ToString();
                    break;
                case DOWORK_CONVERTWMF:
                    break;
            }
            
            this.progressBar1.Value = e.ProgressPercentage;
            LBprecentShow.Text = e.ProgressPercentage.ToString() + "%";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (DoWorkType)
            {
                case DOWORK_CONVERTPNG:
                    if (!e.Cancelled)
                    LBDocxToPNGCounter.Text = "0";
                    dtp.finished();

                break;

                case DOWORK_PACK:
                    if (!e.Cancelled)
                    LBPackCounter.Text = "0";
                break;

                case DOWORK_UNPACK:
                    if (!e.Cancelled)
                    LBUnPackCount.Text = "0";
                break;

                case DOWORK_CONVERTWMF:
                break;
            }

            EnableBottoms();

            if (!e.Cancelled)
            {
                logWrite("\r\n\t **** Finished! :) ****\r\n\r\n");
                DocxListPointer = 0;

            }
            else
            {
                logWrite("\r\n\t **** Stoped! ****\r\n\r\n");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (File.Exists("wordTools_errors.log"))
                File.Delete("wordTools_errors.log");

            errorLog.Text = "0";

            DocxListPointer = 0;

            LBPackCounter.Text = "0";
            LBWMFCounter.Text = "0";
            LBUnPackCount.Text = "0";
            LBDocxToPNGCounter.Text = "0";

            progressBar1.Value = 0;
            LBprecentShow.Text = "0%";
            
            reset.Enabled = false;
            EnableBottoms();
        }
    }
}
