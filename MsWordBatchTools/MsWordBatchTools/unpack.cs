﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ionic.Zip;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.ComponentModel;
using System.Xml;

namespace MsWordBatchTools
{
    class unpack
    {
        private Form1 form;
        private bool chBUnPackExistsAlert;
        public bool radBUnPackAll;

        public unpack(Form1 form, bool radBUnPackAll, bool chBUnPackExistsAlert)
        {
            this.form = form;
            this.chBUnPackExistsAlert = chBUnPackExistsAlert;
            this.radBUnPackAll = radBUnPackAll;
        }

        public void unzipALL(FileInfo docx)
        {

            string destFolder = docx.FullName.Replace(".docx", "");

            if (Directory.Exists(destFolder))
            {
                DialogResult res = DialogResult.Yes;

                if (chBUnPackExistsAlert)
                {
                   res = MessageBox.Show("extracted folder \""+destFolder+"\" \r\n Already exists do you want to replace?",
                       "Extracted folder already exists", 
                       MessageBoxButtons.YesNo
                   );
                }

                if (res == DialogResult.Yes)
                   removeDir(destFolder);
                else
                    return;
            }

            form.logWrite(" ** Trying to open " + docx.Name + "\r\n");

            try
            {
                /* استفاده از یوزینگ کمک میکند که فایلی باز نماند و پروسس بعد از اتمام امن بسته شود*/
                using (ZipFile zip = ZipFile.Read(docx.FullName))
                {
                    try
                    {
                        zip.ExtractAll(destFolder);
                        form.logWrite(" - docx unpacked successfuly\r\n\r\n");
                    }
                    catch (Exception er)
                    {
                        form.ErLogWrite(" - unpack failed!\r\nFile:"+docx.FullName+"\r\nExeption: " + er.Message + "\r\n\r\n");
                    }
                }
            }
            catch (Exception e)
            {
                form.ErLogWrite(" - can't open docx !\r\nFile:" + docx.FullName + "\r\nExeption: " + e.Message + "\r\n\r\n");
            }
        }

        public void unZipDocumentXML(FileInfo docx)
        {
            string destFile = docx.FullName.Replace(".docx", ".xml");

            if (File.Exists(destFile))
            {
                DialogResult res = DialogResult.Yes;

                if (chBUnPackExistsAlert)
                {
                    res = MessageBox.Show("extracted file \"" + destFile + "\" \r\n Already exists do you want to replace?",
                        "Extracted file already exists",
                        MessageBoxButtons.YesNo
                    );
                }

                if (res == DialogResult.Yes)
                {
                    File.Delete(destFile);
                }
                else
                {
                    //t.STOPFlag = true;
                    return;
                }
            }


            form.logWrite(" ** Trying to open " + docx.Name + "\r\n");

            try
            {
                /* استفاده از یوزینگ کمک میکند که فایلی باز نماند و پروسس بعد از اتمام امن بسته شود*/
                using (ZipFile zip = ZipFile.Read(docx.FullName))
                {

                    ZipEntry e = zip["word/document.xml"];

                    form.logWrite(" - Trying to extract\r\n");

                    try{
                        /* ابتدا یک فایل استریم ایجاد میکنیم */
                        using (FileStream fs = File.Create(destFile))
                        {
                            try
                            {
                                /* سپس آن را به تابع اکسترکت میدم تا محتوای فایل مورد نظر را داخل آن رخته و زخیره کند */
                                e.Extract(fs);
                            }
                            catch (Exception er2)
                            {
                                form.ErLogWrite(" - unpack failed!\r\nFile:" + docx.FullName + "\r\nExeption: " + er2.Message + "\r\n\r\n");
                            }

                            form.logWrite(" - document.xml unpacked and saved with new name successfuly\r\n\r\n");

                        }

                        //PrintXML(String XML)
                    }
                    catch (Exception er)
                    {
                        form.ErLogWrite(" - creating xml file failed!\r\nFile:" + destFile  + "\r\nExeption: " + er.Message + "\r\n\r\n");
                    }
                }
            }
            catch (Exception e)
            {
                form.ErLogWrite(" - can't open docx !\r\nFile:" + docx.FullName + "\r\nExeption: " + e.Message + "\r\n\r\n");
            }

        }

        public static String PrintXML(String XML)
        {
            String Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                String FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            document = null;
            mStream.Close();
            writer.Close();

            return Result;
        }

        public void removeDir(string dirName)
        {
            form.logWrite(" - Tying to remove unpacked directory! (its may exists before)\r\n");
            System.IO.DirectoryInfo di = new DirectoryInfo(dirName);

            foreach (FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception ex)
                {
                    form.ErLogWrite(" *** can't delete a file in directory\r\nFile:" + file.Name + "\r\nException:" + ex.Message + "\r\n");
                }

            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete(true);
                }
                catch (Exception ex)
                {
                    form.ErLogWrite(" *** can't delete a directory\r\nException:" + ex.Message + "\r\n");
                }

            }
        }
    
    }
}
