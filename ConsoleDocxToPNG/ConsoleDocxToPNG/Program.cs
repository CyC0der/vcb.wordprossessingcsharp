﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Task2 = System.Threading.Tasks.Task;
using System.Drawing;
using System.IO;

namespace ConsoleDocxToPNG
{
    class Program
    {
        const decimal PNGHeight = 5551;
        const decimal PNGWidth = 2550;

        static void Main(string[] args)
        {

            if (File.Exists("DocxToPNG_error.log"))
            {
                File.Delete("DocxToPNG_error.log");
            }

            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.docx", System.IO.SearchOption.AllDirectories);

            Application app = new Application();

            app.Visible = false;

            foreach (string docx in list)
            {

                convert(docx, app);
            }

            if (app != null)
            {
                app.Quit(false, Type.Missing, Type.Missing);
                Marshal.ReleaseComObject(app);
                app = null;
            }
        }

        public static void convert(string docxStr, Application app)
        {

            FileInfo docx = new FileInfo(docxStr);

            string error;
            string destFolder = docx.FullName.Replace(".docx", "_png");

            if (!Directory.Exists(destFolder))
            {
                try
                {
                    Directory.CreateDirectory(destFolder);

                    Console.WriteLine(" - *_png directory Created.");
                }
                catch (Exception e)
                {
                    error = " *** can't create *_png directory\r\nPath:" + destFolder + "\r\nException:" + e.Message;
                    File.AppendAllText("DocxToPNG_error.log", error);
                    Console.WriteLine(error);
                    return;
                }
            }
            else
            {
                Console.WriteLine(" - *_png directory already exists.\r\n");
            }

            Document doc = new Document();

            try
            {
                doc = app.Documents.Open(docx.FullName);

                Console.WriteLine(" - docx file " + docx.Name + " opened.\r\n");
            }
            catch (Exception er)
            {
                error = " *** can't open docx file\r\nFile:" + docx.FullName + "\r\nException:" + er.Message;
                File.AppendAllText("DocxToPNG_error.log", error);
                Console.WriteLine(error);
            }


            doc.ShowGrammaticalErrors = false;
            doc.ShowRevisions = false;
            doc.ShowSpellingErrors = false;

            Console.WriteLine(" - start looping pages...\r\n");
            //Opens the word document and fetch each page and converts to image
            foreach (Microsoft.Office.Interop.Word.Window window in doc.Windows)
            {
                foreach (Microsoft.Office.Interop.Word.Pane pane in window.Panes)
                {
                    for (var i = 1; i <= pane.Pages.Count; i++)
                    {
                        Console.WriteLine(" - fetching page(" + i.ToString() + ") data\r\n");
                        Microsoft.Office.Interop.Word.Page page = null;
                        bool populated = false;
                        while (!populated)
                        {
                            try
                            {
                                // This !@#$ variable won't always be ready to spill its pages. If you step through
                                // the code, it will always work.  If you just execute it, it will crash.  So what
                                // I am doing is letting the code catch up a little by letting the thread sleep
                                // for a microsecond.  The second time around, this variable should populate ok.
                                page = pane.Pages[i];
                                populated = true;
                            }
                            catch (COMException ex)
                            {
                                Thread.Sleep(1);
                            }
                        }
                        var bits = page.EnhMetaFileBits;
                        var pngTarget = destFolder + "\\" + i.ToString() + ".png";

                        try
                        {
                            Console.WriteLine(" - getting from MemmoryStream page(" + i.ToString() + ")\r\n");
                            using (var ms = new MemoryStream((byte[])(bits)))
                            {
                                Image image = Image.FromStream(ms);
                                Bitmap myBitmap = new Bitmap(image, new Size(Convert.ToInt32(PNGWidth), Convert.ToInt32(PNGHeight)));
                                myBitmap.Save(pngTarget, System.Drawing.Imaging.ImageFormat.Png);
                                Console.WriteLine(" - PNG saved.\r\n");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            error = " *** can't save PNG page(" + i.ToString() + ")\r\nFile:" + docx.FullName + "\r\nException:" + ex.Message;
                            File.AppendAllText("DocxToPNG_error.log", error);
                            Console.WriteLine(error);

                        }
                    }
                }
            }

            doc.Close(false, Type.Missing, Type.Missing);


            Marshal.ReleaseComObject(doc);

            Console.WriteLine(" - docx closed.\r\n\r\n");
        }

    
    }
}
