﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using Ionic.Zip;

namespace ConsoleUnPackDocumentOnly
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists("UnPackDocumentOnly_error.log"))
            {
                File.Delete("UnPackDocumentOnly_error.log");
            }

            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.docx", System.IO.SearchOption.AllDirectories);


            foreach (string docx in list)
            {

                unZipDocumentXML(new FileInfo(docx));
            }

        }

        public  static void unZipDocumentXML(FileInfo docx)
        {
            string destFile = docx.FullName.Replace(".docx", ".xml");

            if (File.Exists(destFile))
            {
                File.Delete(destFile);
            }

            string error;

            Console.WriteLine(" ** Trying to open " + docx.Name + "\r\n");

            try
            {
                /* استفاده از یوزینگ کمک میکند که فایلی باز نماند و پروسس بعد از اتمام امن بسته شود*/
                using (ZipFile zip = ZipFile.Read(docx.FullName))
                {

                    ZipEntry e = zip["word/document.xml"];

                    Console.WriteLine(" - Trying to extract\r\n");

                    try
                    {
                        /* ابتدا یک فایل استریم ایجاد میکنیم */
                        using (FileStream fs = File.Create(destFile))
                        {
                            try
                            {
                                /* سپس آن را به تابع اکسترکت میدم تا محتوای فایل مورد نظر را داخل آن رخته و زخیره کند */
                                e.Extract(fs);
                            }
                            catch (Exception er2)
                            {
                                error = " - unpack failed!\r\nFile:" + docx.FullName + "\r\nExeption: " + er2.Message;
                                File.AppendAllText("UnPackDocumentOnly_error.log", error);
                                Console.WriteLine(error);
                            }

                            Console.WriteLine(" - document.xml unpacked and saved with new name successfuly\r\n\r\n");

                        }
                    }
                    catch (Exception er)
                    {
                        error = " - creating xml file failed!\r\nFile:" + destFile + "\r\nExeption: " + er.Message;
                        File.AppendAllText("UnPackDocumentOnly_error.log", error);
                        Console.WriteLine(error);
                    }
                }
            }
            catch (Exception e)
            {
                error = " - can't open docx !\r\nFile:" + docx.FullName + "\r\nExeption: " + e.Message + "\r\n";
                File.AppendAllText("UnPackDocumentOnly_error.log", error);
                Console.WriteLine(error);
            }

        }

    }
}
