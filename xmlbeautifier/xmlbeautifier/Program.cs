﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace xmlbeautifier
{
    class Program
    {
        static void Main(string[] args)
        {

             string[] list = Directory.GetFiles("docx", "*.xml", System.IO.SearchOption.AllDirectories);

            foreach (string docx in list)
            {
                string Result;

                MemoryStream mStream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
                XmlDocument document = new XmlDocument();

                try
                {
                    // Load the XmlDocument with the XML.
                    document.Load(docx);
                    // document.lo
                    writer.Formatting = Formatting.Indented;

                    // Write the XML into a formatting XmlTextWriter
                    document.WriteContentTo(writer);
                    writer.Flush();
                    mStream.Flush();

                    // Have to rewind the MemoryStream in order to read
                    // its contents.
                    mStream.Position = 0;

                    // Read MemoryStream contents into a StreamReader.
                    StreamReader sReader = new StreamReader(mStream);

                    // Extract the text from the StreamReader.
                    String FormattedXML = sReader.ReadToEnd();

                    Result = FormattedXML;
                }
                catch (XmlException e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

                mStream.Close();
                writer.Close();

                File.WriteAllText(docx, Result);

                Console.WriteLine(docx + " XML Beautifuled!\r\n");
            }

            System.Threading.Thread.Sleep(10000);
        }


    }
}
