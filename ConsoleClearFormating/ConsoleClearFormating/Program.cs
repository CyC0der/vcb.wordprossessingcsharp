﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace ConsoleClearFormating
{
    class Program
    {
        static void Main(string[] args)
        {

            if (File.Exists("ClearFormating_error.log"))
            {
                File.Delete("ClearFormating_error.log");
            }

            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.docx", System.IO.SearchOption.AllDirectories);

            Application app = new Application();

            Document doc;

            app.Visible = false;

            string error;

            foreach (string docx in list)
            {
                try
                {
                    doc = new Document();
                    doc = app.Documents.Open(docx);
                }
                catch (Exception e)
                {
                    error = "can't open file name \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                    File.AppendAllText("ClearFormating_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }

                ClearFormatAndSaveUnderline(app);

                Console.WriteLine("clear Formating:\r\n" + docx + "\r\n");

                try
                {
                    doc.Close(true);
                }
                catch (Exception er)
                {
                    error = "can't close file \r\n" + docx + "\r\nException:" + er.Message + "\r\n";
                    File.AppendAllText("ClearFormating_error.log", error);
                    Console.WriteLine( error);
                }

            }

            app.Quit(true);
        }


        private static void ClearFormatAndSaveUnderline(Application app)
        {
            /* Ctrl+A */
            app.Selection.WholeStory();

            /* Set Selected text highlight to No Color */
            app.Selection.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;

            /* set default text highlight color to green */
            app.Options.DefaultHighlightColorIndex = WdColorIndex.wdBrightGreen;

            /* find all text with  underline and replace with highlighted text (green) */
            app.Selection.Find.ClearFormatting();
            app.Selection.Find.Font.Underline = WdUnderline.wdUnderlineSingle;
            app.Selection.Find.Replacement.ClearFormatting();
            app.Selection.Find.Replacement.Highlight = 1;
            app.Selection.Find.Text = "";
            app.Selection.Find.Replacement.Text = "";
            app.Selection.Find.Forward = true;
            app.Selection.Find.Wrap = WdFindWrap.wdFindContinue;
            app.Selection.Find.Format = true;
            app.Selection.Find.MatchCase = false;
            app.Selection.Find.MatchWholeWord = false;
            app.Selection.Find.MatchKashida = false;
            app.Selection.Find.MatchDiacritics = false;
            app.Selection.Find.MatchAlefHamza = false;
            app.Selection.Find.MatchControl = false;
            app.Selection.Find.MatchWildcards = false;
            app.Selection.Find.MatchSoundsLike = false;

            app.Selection.Find.Execute(
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                WdReplace.wdReplaceAll
            );


            /* ClearFormat of Document text */
            app.Selection.WholeStory();
            app.Selection.ClearFormatting();

            /* find all text with  text highlighted and replace with underline text  */
            app.Selection.Find.ClearFormatting();
            app.Selection.Find.Highlight = 1;
            app.Selection.Find.Replacement.ClearFormatting();
            app.Selection.Find.Replacement.Font.Underline = WdUnderline.wdUnderlineSingle;
            app.Selection.Find.Text = "";
            app.Selection.Find.Replacement.Text = "";
            app.Selection.Find.Forward = true;
            app.Selection.Find.Wrap = WdFindWrap.wdFindContinue;
            app.Selection.Find.Format = true;
            app.Selection.Find.MatchCase = false;
            app.Selection.Find.MatchWholeWord = false;
            app.Selection.Find.MatchKashida = false;
            app.Selection.Find.MatchDiacritics = false;
            app.Selection.Find.MatchAlefHamza = false;
            app.Selection.Find.MatchControl = false;
            app.Selection.Find.MatchWildcards = false;
            app.Selection.Find.MatchSoundsLike = false;

            app.Selection.Find.Execute(
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                WdReplace.wdReplaceAll
            );

            /* remove background color */
            app.Options.DefaultHighlightColorIndex = WdColorIndex.wdNoHighlight;
            app.Selection.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;
        }
    }
}
