﻿namespace WordTableToBreakPage
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chBAError = new System.Windows.Forms.CheckBox();
            this.chLQuestSplit = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chLPhraseSplit = new System.Windows.Forms.CheckedListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtLogView = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.selectPath = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.folderPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chBDoNotEdit = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chBDetectImagePhrase = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRiaziFolder = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LBDocx = new System.Windows.Forms.Label();
            this.errorLog = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LBPrecentShow = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.LBScanned = new System.Windows.Forms.Label();
            this.chBXMLFile = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chBAError
            // 
            this.chBAError.AutoSize = true;
            this.chBAError.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.chBAError.Location = new System.Drawing.Point(20, 170);
            this.chBAError.Name = "chBAError";
            this.chBAError.Size = new System.Drawing.Size(215, 17);
            this.chBAError.TabIndex = 1;
            this.chBAError.Text = "در صورت نبودن فایل جواب یک خطا ثبت کن";
            this.chBAError.UseVisualStyleBackColor = true;
            // 
            // chLQuestSplit
            // 
            this.chLQuestSplit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.chLQuestSplit.FormattingEnabled = true;
            this.chLQuestSplit.Items.AddRange(new object[] {
            "بر اساس متن (bm)",
            "جورکردنی (jk)",
            "کوتاه پاسخ (kp)",
            "صحیح و غلط (tf)",
            "تشریحی (th)",
            "تستی (ts)"});
            this.chLQuestSplit.Location = new System.Drawing.Point(264, 44);
            this.chLQuestSplit.Name = "chLQuestSplit";
            this.chLQuestSplit.Size = new System.Drawing.Size(246, 116);
            this.chLQuestSplit.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(17, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "اجرای جدا کننده عبارت :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(261, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(239, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "تقسیم عبارت به سوال های جدید با عنوان تکراری :";
            // 
            // chLPhraseSplit
            // 
            this.chLPhraseSplit.CheckOnClick = true;
            this.chLPhraseSplit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.chLPhraseSplit.FormattingEnabled = true;
            this.chLPhraseSplit.Items.AddRange(new object[] {
            "بر اساس متن (bm)",
            "جورکردنی (jk)",
            "کوتاه پاسخ (kp)",
            "صحیح و غلط (tf)",
            "تشریحی (th)",
            "تستی (ts)"});
            this.chLPhraseSplit.Location = new System.Drawing.Point(20, 44);
            this.chLPhraseSplit.Name = "chLPhraseSplit";
            this.chLPhraseSplit.Size = new System.Drawing.Size(228, 116);
            this.chLPhraseSplit.TabIndex = 5;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(48, 337);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(429, 23);
            this.progressBar1.TabIndex = 6;
            // 
            // txtLogView
            // 
            this.txtLogView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtLogView.Location = new System.Drawing.Point(12, 30);
            this.txtLogView.Name = "txtLogView";
            this.txtLogView.Size = new System.Drawing.Size(465, 293);
            this.txtLogView.TabIndex = 7;
            this.txtLogView.Text = "";
            this.txtLogView.TextChanged += new System.EventHandler(this.txtLogView_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(497, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "آدرس فایل های ورودی:";
            // 
            // selectPath
            // 
            this.selectPath.Location = new System.Drawing.Point(826, 28);
            this.selectPath.Name = "selectPath";
            this.selectPath.Size = new System.Drawing.Size(75, 23);
            this.selectPath.TabIndex = 9;
            this.selectPath.Text = "انتخاب ...";
            this.selectPath.UseVisualStyleBackColor = true;
            this.selectPath.Click += new System.EventHandler(this.selectPath_Click);
            // 
            // Reset
            // 
            this.Reset.Enabled = false;
            this.Reset.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reset.Location = new System.Drawing.Point(581, 327);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 47);
            this.Reset.TabIndex = 10;
            this.Reset.Text = "بازنشانی";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // stop
            // 
            this.stop.Enabled = false;
            this.stop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop.Location = new System.Drawing.Point(500, 327);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 47);
            this.stop.TabIndex = 11;
            this.stop.Text = "توقف";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // start
            // 
            this.start.Enabled = false;
            this.start.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start.Location = new System.Drawing.Point(662, 327);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 47);
            this.start.TabIndex = 12;
            this.start.Text = "شروع";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // folderPath
            // 
            this.folderPath.Location = new System.Drawing.Point(499, 30);
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(321, 21);
            this.folderPath.TabIndex = 13;
            this.folderPath.TextChanged += new System.EventHandler(this.folderPath_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chBDoNotEdit);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.chBDetectImagePhrase);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtRiaziFolder);
            this.groupBox1.Controls.Add(this.chLQuestSplit);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chLPhraseSplit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chBAError);
            this.groupBox1.Location = new System.Drawing.Point(500, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 245);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "کنترل عملیات";
            // 
            // chBDoNotEdit
            // 
            this.chBDoNotEdit.AutoSize = true;
            this.chBDoNotEdit.Location = new System.Drawing.Point(20, 217);
            this.chBDoNotEdit.Name = "chBDoNotEdit";
            this.chBDoNotEdit.Size = new System.Drawing.Size(125, 17);
            this.chBDoNotEdit.TabIndex = 10;
            this.chBDoNotEdit.Text = "فایل ها را ویرایش نکن";
            this.chBDoNotEdit.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(271, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(186, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "(تمامی پوشته هایی که شامل این نام )";
            // 
            // chBDetectImagePhrase
            // 
            this.chBDetectImagePhrase.AutoSize = true;
            this.chBDetectImagePhrase.Checked = true;
            this.chBDetectImagePhrase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBDetectImagePhrase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.chBDetectImagePhrase.Location = new System.Drawing.Point(20, 193);
            this.chBDetectImagePhrase.Name = "chBDetectImagePhrase";
            this.chBDetectImagePhrase.Size = new System.Drawing.Size(156, 17);
            this.chBDetectImagePhrase.TabIndex = 8;
            this.chBDetectImagePhrase.Text = "نوع شکل دار را تشخیص بده ";
            this.chBDetectImagePhrase.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.Location = new System.Drawing.Point(272, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "پوشه ریاضی:";
            // 
            // txtRiaziFolder
            // 
            this.txtRiaziFolder.Location = new System.Drawing.Point(275, 189);
            this.txtRiaziFolder.Name = "txtRiaziFolder";
            this.txtRiaziFolder.Size = new System.Drawing.Size(100, 21);
            this.txtRiaziFolder.TabIndex = 6;
            this.txtRiaziFolder.Text = "riazi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(916, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "خطا ها";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(916, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "انتخاب شده:";
            // 
            // LBDocx
            // 
            this.LBDocx.AutoSize = true;
            this.LBDocx.Location = new System.Drawing.Point(995, 27);
            this.LBDocx.Name = "LBDocx";
            this.LBDocx.Size = new System.Drawing.Size(13, 13);
            this.LBDocx.TabIndex = 18;
            this.LBDocx.Text = "0";
            // 
            // errorLog
            // 
            this.errorLog.AutoSize = true;
            this.errorLog.Location = new System.Drawing.Point(995, 50);
            this.errorLog.Name = "errorLog";
            this.errorLog.Size = new System.Drawing.Size(13, 13);
            this.errorLog.TabIndex = 19;
            this.errorLog.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "نمایش گزارش :";
            // 
            // LBPrecentShow
            // 
            this.LBPrecentShow.AutoSize = true;
            this.LBPrecentShow.Location = new System.Drawing.Point(9, 341);
            this.LBPrecentShow.Name = "LBPrecentShow";
            this.LBPrecentShow.Size = new System.Drawing.Size(24, 13);
            this.LBPrecentShow.TabIndex = 21;
            this.LBPrecentShow.Text = "0%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(772, 344);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "فایل های تغییر کرده:";
            // 
            // LBScanned
            // 
            this.LBScanned.AutoSize = true;
            this.LBScanned.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.LBScanned.Location = new System.Drawing.Point(876, 334);
            this.LBScanned.Name = "LBScanned";
            this.LBScanned.Size = new System.Drawing.Size(29, 31);
            this.LBScanned.TabIndex = 23;
            this.LBScanned.Text = "0";
            this.LBScanned.Click += new System.EventHandler(this.LBScanned_Click);
            // 
            // chBXMLFile
            // 
            this.chBXMLFile.AutoSize = true;
            this.chBXMLFile.Location = new System.Drawing.Point(500, 53);
            this.chBXMLFile.Name = "chBXMLFile";
            this.chBXMLFile.Size = new System.Drawing.Size(182, 17);
            this.chBXMLFile.TabIndex = 24;
            this.chBXMLFile.Text = "فایل های ایکس ام ال را انتخاب کن";
            this.chBXMLFile.UseVisualStyleBackColor = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(354, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Code By Kourosh Raoufi";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(955, 367);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "                          ";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-1, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "                   ";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 383);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chBXMLFile);
            this.Controls.Add(this.LBScanned);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.LBPrecentShow);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.errorLog);
            this.Controls.Add(this.LBDocx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.start);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.folderPath);
            this.Controls.Add(this.selectPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLogView);
            this.Controls.Add(this.progressBar1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "برنامه اختصاصی تبدیل محتوای پروژه آزمون ساز - مرحله تقسیم بندی صفحات ورد";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chBAError;
        private System.Windows.Forms.CheckedListBox chLQuestSplit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox chLPhraseSplit;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.RichTextBox txtLogView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button selectPath;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.TextBox folderPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LBDocx;
        private System.Windows.Forms.Label errorLog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LBPrecentShow;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtRiaziFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label LBScanned;
        private System.Windows.Forms.CheckBox chBDetectImagePhrase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chBXMLFile;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chBDoNotEdit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;

    }
}

