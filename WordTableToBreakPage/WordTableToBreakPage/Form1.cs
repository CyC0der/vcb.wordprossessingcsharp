﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;


namespace WordTableToBreakPage
{
    public partial class Form1 : Form
    {

        XmlNodeList MainQTable;
        XmlNodeList MainATable;
        XmlNode QwBody;
        XmlNode AwBody;
        XmlNode ASectPr;
        XmlNode QSectPr;
        XmlNode QMainRow;
        XmlNode AMainRow;
        XmlNode subQTable;
        XmlNode subATable;
        XmlNamespaceManager mq;
        XmlNamespaceManager ma;
        int MainQTableCount;
        int MainATableCount;
        bool flagAnswerFileExists;

        int ErrorCounter = 0;
        public string[] FileList;
        public int FileListPointer = 0;

        public Form1()
        {
            InitializeComponent();

            if (File.Exists("wordTableToBreakPage_errors.log"))
                File.Delete("wordTableToBreakPage_errors.log");

            /* فعال کردن این دو فلگ باعث میشود ترید ما مارا از پیش روی پروسس مطلع سازد*/
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            /* Binding events to the  background worker */
            /* code to run */
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            /* invloked when proccess end */
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            /* invoked when worker.ReportProgress() used in  doWork */
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;

            /* این فایل حاوی آخرین آدرس فولد انتخاب شده به وسیله کاربر است */
            if (File.Exists("3conf.ini"))
                folderPath.Text = System.IO.File.ReadAllText("3conf.ini");

            chLQuestSplit.SetItemChecked(5, true);

            chLPhraseSplit.SetItemChecked(0, true);
            chLPhraseSplit.SetItemChecked(1, true);
            chLPhraseSplit.SetItemChecked(2, true);
            chLPhraseSplit.SetItemChecked(4, true);
        }

        /* این تابع در سر تا سر برنامه اجرا میشود و کارش نمایش گزارش کار در فرم است*/
        public void logWrite(string log)
        {

            /* دلیل استفاده از این متد این است که دسترسی و وارد کردن مقدار از داخل توابع ورکر به کنتزل های فرم به شکل معمولی ممکن نیست*/
            this.Invoke(new MethodInvoker(delegate
            {
                // Execute the following code on the GUI thread.
                txtLogView.Text += log;
            }));
        }

        public void ErLogWrite(string log)
        {
            logWrite(log);

            using (StreamWriter sw = File.AppendText("wordTableToBreakPage_errors.log"))
            {
                sw.WriteLine(log);
            }

            ErrorCounter++;

            this.Invoke(new MethodInvoker(delegate
            {
                errorLog.Text = ErrorCounter.ToString();
            }));
        }

        private void workingUIMode()
        {
            stop.Enabled = true;
            Reset.Enabled = false;
            start.Enabled = false;
            folderPath.Enabled = false;
            selectPath.Enabled = false;
        }

        private void freeUIMode()
        {
            Reset.Enabled = true;
            start.Enabled = true;
            folderPath.Enabled = true;
            selectPath.Enabled = true;
            stop.Enabled = false;
        }

        public void resetFileList()
        {
            start.Enabled = false;
            LBDocx.Text = "0";
            LBScanned.Text = "0";
            FileList = null;
            FileListPointer = 0;
        }

        /*  این متد صحت آدرس انتخاب شده و اینکه آیا فایل ورد در آن است یا نه را چک کرده و لیست فایل های ورد را داخل یک آرایه رشته میریزد*/
        public bool fetchFileList()
        {

            string[] list;

            resetFileList();

            if (!Directory.Exists(folderPath.Text))
            {
                logWrite("** selected directory not exists \r\n\r\n");
                start.Enabled = false;
                return false;
            }

            try
            {
                string fileType = "q?.docx";
                if (chBXMLFile.Checked)
                    fileType = "q?.xml";

                list = System.IO.Directory.GetFiles(folderPath.Text, fileType, System.IO.SearchOption.AllDirectories);
            }
            catch (Exception er)
            {
                logWrite("** can't access to selected directory! \r\n\r\n");
                start.Enabled = false;
                return false;
            }


            LBDocx.Text = list.Length.ToString();

            if (list.Length != 0)
            {
                System.IO.File.WriteAllText("3conf.ini", folderPath.Text);

                this.FileList = list;

                start.Enabled = true;

                return true;
            }

            start.Enabled = false;
            return false;
        }
        
        public void XMLBreakPage()
        {
            XmlNode wBreakPage = ("<w:p xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" ><w:r><w:br w:type=\"page\" /></w:r></w:p>").StringToXmlNode();

            QwBody.AppendChild(QwBody.OwnerDocument.ImportNode(wBreakPage, true));

            if (flagAnswerFileExists)
                AwBody.AppendChild(AwBody.OwnerDocument.ImportNode(wBreakPage, true));

        }

        public void AddQuestionRowToPage() {
           QwBody.AppendListChild(QMainRow.SelectNodes("./*", mq), 1);

            if (flagAnswerFileExists)
                AwBody.AppendListChild(AMainRow.SelectNodes("./*", ma), 1);
        }

        public bool tryToFindNo(XmlNode nodes)
        {
            string str = nodes.ToString(0);

            if (str.IndexOf("@#$") == -1 )
            {
                return false;
            }

            return true;
        }

        public bool tryToFindNo(XmlNodeList nodes)
        {
            int len = nodes.Count;
            for (int i=0; i < len; i++)
            {
                if (tryToFindNo(nodes.Item(i)))
                    return true;
            }

            return false;
        }

        public static bool pathContainsQType(string s,string f)
        {
            string[] slice = s.Split('\\');

            if (slice[slice.Length - 2] == f)
                return true;
            else
                return false;

        }

        private void run_once()
        {

            /* ساخت شی ایکس ام ال  */ 
            XmlDocument xq = new XmlDocument();

            /* بخاطر وجود دو نیم اسپیس که ما آنها را فراخوانی میکنیم نیاز است به شیوه زیر
             * از منیجر استفاده شود و منیچر مورد نظر در طول استفاده از برنامه مورده استفاده قرار میگیرد*/

            mq = new XmlNamespaceManager(xq.NameTable);
            mq.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
             mq.AddNamespace("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
   
            XmlDocument xa = new XmlDocument();
            ma = new XmlNamespaceManager(xa.NameTable);
            ma.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            /* این فلگ مربوط به وجود داشتن یا نداشتن فایل پاسخنامه برای هر فایل سوال است */
            flagAnswerFileExists = false;

            /* این متغییر برای متوفق نشدن اسکریپ در هنگام ارور است و در شرایطی که خطایی رخ میدهد ترو میشود */
            bool detectError = false;

           string QFile = Path.ChangeExtension((new FileInfo(FileList[FileListPointer])).FullName,"xml");
           string AFile = QFile.Replace("q", "a");

            /* نبود فایل سوال خطای مهمی است */
           if (!File.Exists(QFile))
           {
               logWrite("\r\n\r\n فایل ایکس ام ال  سوال موجود نیست! \r\n"+QFile+"\r\n");
               return;
           }

           try
           {
               xq.Load(QFile);
           }
           catch (Exception er)
           {
               ErLogWrite("\r\n\r\n *** محتوای فایل سوال معتبر نیست \r\nFile: \r\n" + QFile + "\r\nException:" + er.Message + "\r\n");
               return;
           }

            /* اگر فایل پاسخنامه  وجود نداشته باشن مهم نیست اما در صورت انتخاب
             * اپشن خطا را ثبت کن در لیست خطاها ثبت میشود                           */
            if (File.Exists(AFile))
            {
                try{
                    xa.Load(AFile);
                }
                catch(Exception er) {
                    ErLogWrite("\r\n\r\n *** محتوای فایل پاسخ معتبر نیست\r\nFile: \r\n" + AFile + "\r\nException:" + er.Message + "\r\n");
                    return;
                }

                flagAnswerFileExists = true;
            }
            else
            {
                if(chBAError.Checked)
                    ErLogWrite("\r\n\r\n *** @1  این فایل سوال ، پاسخنامه ندارد \r\nنام\r\n" + QFile);
                else
                    logWrite("\r\n\r\n - @1 این فایل سوال ، پاسخنامه ندارد \r\nنام\r\n" + QFile);

            }

            
            /* تمامی سطر های اولین جدول موجود در فایل ایکس ام ال را بر میگرداند  
             * که هر سطر شامل یک سوال است */
            MainQTable = xq.SelectNodes("*/w:body/w:tbl/w:tr/w:tc", mq);

            MainQTableCount = MainQTable.Count;

            /* خب اگر جدید باشد یعنی جدولی نیست! */
            if (MainQTableCount == 0)
            {
                ErLogWrite("\r\n\r\n*** جدول اصلی در فایل سوال نیست!! \r\n" + QFile + "\r\n");
                return;
            }

            if (flagAnswerFileExists)
            {

                MainATable = xa.SelectNodes("*/w:body/w:tbl/w:tr/w:tc", ma);


                MainATableCount = MainATable.Count;

                if (MainATableCount == 0)
                {
                    ErLogWrite("\r\n\r\n*** جدول اصلی در فایل پاسخ نیست!! \r\n" + AFile + "\r\n");
                    return;
                }

                if (MainATableCount != MainQTableCount)
                {
                    ErLogWrite("\r\n\r\n*** @2 تعداد سطر های جدول سوال و پاسخ برابر نیستند! \r\n" + QFile + "\r\n");
                    return;
                }

                /* تمامی محتویات داخل بدنه ایکس ام ال را خالی میکنیم
                 * تا با محتوای مورد نظر خودمان از اول پر کنیم          */
                AwBody = xa.SelectSingleNode("*/w:body", ma);
                ASectPr = xa.SelectSingleNode("//w:sectPr",ma).CloneNode(true);
                AwBody.RemoveAll();
            }

            /* تمامی محتویات داخل بدنه ایکس ام ال را خالی میکنیم
             * تا با محتوای مورد نظر خودمان از اول پر کنیم              */
            QwBody = xq.SelectSingleNode("*/w:body", mq);
            QSectPr = xq.SelectSingleNode("//w:sectPr", mq).CloneNode(true);
            QwBody.RemoveAll();

            /* این متغییر در پایین تر شامل تعداد جدول های داخل سطر هر سوال در فایل میشود */
            int tableCount = 0;

            /* اگر این متغییر ترو شود یعنی در این نوع سوال عبارت ها باید با تکرار شدن عنوان سوالشان
             * تبدیل به سوال جدید شوند */
            bool SplitPhraseToQuest = false;

            /* در برابر هر سطر سوال این حلقه یک بار میچرخد */
            for (int i = 0; i < MainQTableCount; i++)
            {
                /* شامل محتوای هر سطر سوال */
                QMainRow = MainQTable.Item(i);

                /* تعداد جدول های داخل یک سطر سوال */
                tableCount = QMainRow.SelectNodes("w:tbl", mq).Count;

                if (flagAnswerFileExists)
                    AMainRow = MainATable.Item(i);


                /* اگر این سوال از نوع  بر اساس متن باشد شرط اجرا می شود*/
                if (pathContainsQType(QFile, "bm"))
                {
                    /* اگر آبشن تبدیل عبارت ها به سوال برای این نوع (بر اساس متن) فعال باشد اجرا میشود */
                    if (chLQuestSplit.GetItemChecked(0))
                        SplitPhraseToQuest = true;
                    /* اگر حالت قبل نباشد و  تیک جدا شدن عبارت برای این نوع فعال نباشد این شرط برقرار است */
                    else if (!chLPhraseSplit.GetItemChecked(0))    
                        tableCount = 0; /* صفر کردن این متغییر در جلوتر از تقسیم عبارت های سوال جلوگیری می کند */
                }

                if (pathContainsQType(QFile, "jk"))
                {
                    if (chLQuestSplit.GetItemChecked(1))
                        SplitPhraseToQuest = true;
                    else if (!chLPhraseSplit.GetItemChecked(1))
                        tableCount = 0;
                }

                if (pathContainsQType(QFile, "kp"))
                {
                   if (chLQuestSplit.GetItemChecked(2))
                        SplitPhraseToQuest = true;
                   else if (!chLPhraseSplit.GetItemChecked(2))
                        tableCount = 0;
                }

                if (pathContainsQType(QFile, "tf"))
                {
                    if (chLQuestSplit.GetItemChecked(3))
                        SplitPhraseToQuest = true;
                    else if (!chLPhraseSplit.GetItemChecked(3))
                        tableCount = 0;
                }

                if (pathContainsQType(QFile, "th"))
                {
                    if (chLQuestSplit.GetItemChecked(4))
                        SplitPhraseToQuest = true;
                    else if (!chLPhraseSplit.GetItemChecked(4))
                        tableCount = 0;
                }

                if (pathContainsQType(QFile, "ts"))
                {
                    if (chLQuestSplit.GetItemChecked(5))
                        SplitPhraseToQuest = true;
                    else if (!chLPhraseSplit.GetItemChecked(5))
                        tableCount = 0;
                }

                bool isZaBan = false;
                if (QFile.IndexOf("zaban") != -1 || QFile.IndexOf("Zaban") != -1)
                {
                    isZaBan = true;
                }

                /*
                  1. اگر آپشن تبدیل عبارت ها به سوال جدید برای این نوع فعال باشد
                  2. در داخل سوال جدولی حاوی عبارت وجود داشته باشد
                  3. اولین یا دومین جدول موجود در سوال تک ستونه باشد
                 */
                if (SplitPhraseToQuest && 
                    tableCount !=0 &&
                    QMainRow.SelectNodes("w:tbl[" + tableCount + "]/w:tr[1]/w:tc", mq).Count == 1
                 ) {
                    /* پس شروع کن به تقسیم عبارت های جدول تک ستونه به سوال جدید با عنوان تکراری */

                    /* آخرین جدول موجود در سوال را انتخاب کن */
                     subQTable = QMainRow.SelectSingleNode("w:tbl[" + tableCount + "]", mq);
                    /* جدول را از فایل حذف کن ولی کپی آن را در خروجی نگهر دار */
                     subQTable = subQTable.ParentNode.RemoveChild(subQTable);
                    /*  تمامی خانه های جدول را انتخاب کن*/
                     XmlNodeList QNodes = subQTable.SelectNodes("w:tr/w:tc", mq);
                     XmlNodeList ANodes = null;

                     int len = QNodes.Count;

                    if (flagAnswerFileExists)
                    {
                        /* در پاسخنامه ببین در سطر جواب جدولی هست */
                        subATable = AMainRow.SelectSingleNode("w:tbl", ma);
                        /* اگر هست شرط برقرار است */
                        if (subATable != null)
                        {
                            /* جدول  را پاک کن */
                            subATable = subATable.ParentNode.RemoveChild(subATable);
                            /* خانه های داخل جدول پاک شده را انتخاب کن */
                            ANodes = subATable.SelectNodes("w:tr/w:tc", ma);

                            /*چک کن ببین خانه های داخل جدول انتخاب شده با تعداد خانه های جدول سوال برابر است  */
                            if (ANodes.Count != QNodes.Count)
                            {
                                ErLogWrite("\r\n\r\n*** @3 تعداد سطرهای سوال های تقسیمی در جدول فایل سوال و فایل پاسخ برابر نیستند شماره سطر جدول اصلی " + (i + 1).ToString() + " \r\n" + QFile + "\r\n");
                                detectError = true;
                            }
                        }
                        /* اگر جدولی نبود ببین داخل سطر جواب کارکتر نشان دهنده این پاسخنامه جواب ندارد موجود است */
                        else if (!tryToFindNo(AMainRow))
                        {
                                /* اگر نیست خطا بده */
                                ErLogWrite("\r\n\r\n*** @4 جواب سطرهای سوال های تقسیمی در فایل جواب یافت نشد شماره سطر فایل " + (i + 1).ToString() + " \r\n" + QFile + "\r\n");
                                detectError = true;
                        }
                    }

                    /* شروع کن به مرور کردن سطر های جدول عبارت */
                     for (int j = 0; j < len; j++)
                     {
                         /* اگر عبارت جواب داشت */
                         if (flagAnswerFileExists && subATable != null && ANodes!=null)
                         {
                             AwBody.AppendListChild(AMainRow.SelectNodes("./*", ma), 1);
                             AwBody.AppendListChild(ANodes.Item(j).SelectNodes("./*", ma), 1);
                         }

                         /* عنوان سوال را تکرار کن */
                         QwBody.AppendListChild(QMainRow.SelectNodes("./*", mq), 1);
                         /* و عبارت ها را یکی  یکی بچین زیرش */
                         QwBody.AppendListChild(QNodes.Item(j).SelectNodes("./*", mq), 1);

                         /* کارکتر بریک پیج را اضافه کن به غیر از آخرین */
                         if ((j + 1) < len)
                             XMLBreakPage(); 
                     }
                }
                else
                    switch (tableCount)
                    {
                        /* اگر جدولی در سوال نیست پس عبارتی هم نیست */
                        case 0:
                            /* کل سوال را بدون تلاش برای پیدا کردن عبارت کپی کن */
                            AddQuestionRowToPage();
                        break;

                        case 1:
                            /* اگر تعداد ستون های داخل جدول بیشتر از یکی بود */
                            if (QMainRow.SelectNodes("w:tbl/w:tr[1]/w:tc", mq).Count != 1)
                            {
                                    
                                if (QMainRow.SelectNodes("w:tbl/w:tr/w:tc/w:tbl", mq).Count != 0 || /* <-- اگر در یکی از خانه های جدول ، جدول دیگری بود  */
                                    pathContainsQType(QFile, "jk") ||                                /* <-- اگر جور کردنی بود */
                                    pathContainsQType(QFile, "bm") ||                               /* <-- اگر بر اساس متن بود */
                                    QFile.IndexOf("riazi") == -1                                    /* <-- اگر درس ریاضی نبود */
                                ) {
                                    /* سوال را بدون تغییر اضافه کن */
                                    AddQuestionRowToPage();
                                    break;
                                }
                               
                            }

                           //XmlNodeList hasJpg = QMainRow.SelectNodes("w:p/w:r/w:drawing/wp:inline/wp:docPr[@descr=substring(@descr, string-length(@descr) - string-length('.jpg') +1) = '.jpg']", mq);
                            /*XmlNodeList hasPng = QMainRow.SelectNodes("w:p/w:r/w:drawing/wp:inline/wp:docPr[@id]", mq);

                            int a = hasPng.Count;
                            if (a != 0)
                                MessageBox.Show(a.ToString() + " " + (i+1) + " in file:" + QFile);*/

         

                            /* از این به بعد جدول را بشکاف */

                            if (flagAnswerFileExists)
                            {
                                /*  تعداد خانه های جدول عبارت پاسخنامه را بگیر */
                                int cellCount = QMainRow.SelectNodes("w:tbl/w:tr/w:tc", mq).Count;

                                /* اگر عبارت یک خانه نداشت و یا کارکتر سوال پاسخ ندارد در آن نبود اجرا کن  */
                                if (cellCount != 1 && !tryToFindNo(AMainRow))
                                {
                                    /* اگر در داخل فایل سطر پاسخ جدولی نبود */
                                    if (AMainRow.SelectSingleNode("w:tbl", ma) == null )
                                    {
                                        ErLogWrite("\r\n\r\n*** @5 جدول حاوی عبارت در سطر " + (i + 1).ToString() + " فایل پاسخ وجود ندارد \r\n" + QFile + "\r\n");
                                        detectError = true;
                                        break;
                                    } 
                                   /* اگر جدولی بود اما تعداد خانه های آن با جدول عبارت برابر نبود */
                                    else if (QMainRow.SelectNodes("w:tbl/w:tr/w:tc", mq).Count != AMainRow.SelectNodes("w:tbl/w:tr/w:tc", ma).Count)
                                    {
                                        ErLogWrite("\r\n\r\n*** @6 تعداد سطر های جدول عبارت سوال و پاسخ در سطر " + (i + 1).ToString() + " برابر نیستند  \r\n" + QFile + "\r\n");
                                        detectError = true;
                                        break;
                                    }

                                    /* جواب های موجود در سطر ها را با خط قرمز جدا کن */
                                    AwBody.splitWithRedLine(AMainRow, 1,isZaBan, ma);
                                }
                                else
                                    /* فقط محتوای داخل سطر پاسخ را کپی کن */
                                    AwBody.AppendListChild(AMainRow.SelectNodes("./*", ma), 1);
                            }

                            /* جواب های موجود در سطر ها را با خط قرمز جدا کن */
                            QwBody.splitWithRedLine(QMainRow, 1,isZaBan, mq);
                        break;

                        /*  در داخل سطر این سوال بیش از یک جدول قرار دارد*/
                        case 2:

                            if (QMainRow.SelectNodes("w:tbl[2]/w:tr[1]/w:tc", mq).Count != 1) {
                                /* جدول دوم بیشتر از یک ستون دارد پس بیخیال گشتن دنبال عبارت بشو */
                                AddQuestionRowToPage();
                                break;
                            }

                            if (flagAnswerFileExists)
                            {
                                int cellCount = QMainRow.SelectNodes("w:tbl[2]/w:tr/w:tc", mq).Count;

                                if (cellCount != 1 &&  !tryToFindNo(AMainRow))
                                {
                                    if (AMainRow.SelectSingleNode("w:tbl", ma) == null )
                                    {
                                        ErLogWrite("\r\n\r\n*** @5 جدول حاوی عبارت در سطر " + (i + 1).ToString() + " فایل پاسخ وجود ندارد \r\n" + QFile + "\r\n");
                                        detectError = true;
                                        break;
                                    }
                                    else if (QMainRow.SelectNodes("w:tbl[2]/w:tr/w:tc", mq).Count != AMainRow.SelectNodes("w:tbl/w:tr/w:tc", ma).Count)
                                    {
                                        ErLogWrite("\r\n\r\n*** @6 تعداد سطر های جدول عبارت سوال و پاسخ در سطر " + (i + 1).ToString() + " برابر نیستند  \r\n" + QFile + "\r\n");
                                        detectError = true;
                                        break;
                                    }

                                    AwBody.splitWithRedLine(AMainRow, 1,isZaBan, ma);
                                }
                                else
                                    AwBody.AppendListChild(AMainRow.SelectNodes("./*", ma), 1);

                            }

                            QwBody.splitWithRedLine(QMainRow, 2,isZaBan, mq);
                        break;

                        default:
                            ErLogWrite("\r\n\r\n*** @7 بیش از دو جدل در سوال قرار دارد سطر شماره " + (i + 1).ToString() + " در فایل \r\n" + QFile + "\r\n");
                            detectError = true;
                            break;
                    }


                if ((i + 1) < MainQTableCount)
                    XMLBreakPage();
            }

             if (flagAnswerFileExists)
                 xa.SelectSingleNode("//w:body",ma).AppendChild(ASectPr);

             xq.SelectSingleNode("//w:body",mq).AppendChild(QSectPr);

            logWrite("\r\nفایل باز شد  حاوی " + MainQTableCount.ToString() + " سوال ");

            if (!chBDoNotEdit.Checked && !detectError)
            {
                try
                {
                    System.IO.File.WriteAllText(QFile, xq.OuterXml);
                }
                catch (Exception e)
                {
                    ErLogWrite("\r\n\r\nفایل سوال زخیره نشد ! \r\n" + QFile + "\r\nException:\r\n " + e.Message  +"\r\n");
                }

                if(flagAnswerFileExists)
                    try
                    {
                        System.IO.File.WriteAllText(AFile, xa.OuterXml);
                    }
                    catch (Exception e)
                    {
                        ErLogWrite("\r\n\r\nفایل جواب زخیره نشد ! \r\n" + AFile + "\r\nException:\r\n " + e.Message + "\r\n");
                    }
            }


            xa = null;
            xq = null;
            MainQTable = null;
            MainATable = null;
            QwBody = null;
            AwBody = null;
            QMainRow = null;
            AMainRow = null;
            subQTable = null;
            subATable = null;

        }
        
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            int dLen = FileList.Length;

            for (; FileListPointer < dLen;FileListPointer++)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                run_once();

                //Math.Ceiling(Convert.ToInt32(
                worker.ReportProgress(Convert.ToInt32(((double)FileListPointer * 100) / (double)dLen));
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar1.Value = e.ProgressPercentage;
            LBPrecentShow.Text = e.ProgressPercentage.ToString() + "%";
            LBScanned.Text = FileListPointer.ToString();

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            freeUIMode();

            if (!e.Cancelled)
            {
                logWrite("\r\n\t **** Finished! :) ****\r\n\r\n");
                FileListPointer = 0;

                return;
            }

            logWrite("\r\n\t **** Stoped! ****\r\n\r\n");

        }

        private void stop_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                backgroundWorker1.CancelAsync();
            }

            freeUIMode();
        }

        private void start_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                //cf = new clearformat(this, chBCostumNext.Checked, txtDelay.Value, chBVisibleWord.Checked);

                logWrite("\r\n\t **** Start Converting ! ****\r\n\r\n");

                workingUIMode();


                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            if (File.Exists("wordTableToBreakPage_errors.log"))
                File.Delete("wordTableToBreakPage_errors.log");
            
            freeUIMode();
            FileListPointer = 0;
            progressBar1.Value = 0;
            LBPrecentShow.Text = "0%";
            LBDocx.Text = "0";
            Reset.Enabled = false;
        }

        private void selectPath_Click(object sender, EventArgs e)
        {
            /* if user click on OK then */
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath.Text = folderBrowserDialog1.SelectedPath;
                resetFileList();
            }
        }

        private void txtLogView_TextChanged(object sender, EventArgs e)
        {
            txtLogView.SelectionStart = txtLogView.Text.Length; //Set the current caret position at the end
            txtLogView.ScrollToCaret(); //Now scroll it automatically
        }

        private void folderPath_TextChanged(object sender, EventArgs e)
        {
            resetFileList();

            if (!Directory.Exists(folderPath.Text))
                return;

            fetchFileList();
        }

        private void LBScanned_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            MessageBox.Show("گوگولی :)");
        }

        private void label9_Click(object sender, EventArgs e)
        {
            MessageBox.Show(":) نترس کاریت ندارم");
        }

    }
}
