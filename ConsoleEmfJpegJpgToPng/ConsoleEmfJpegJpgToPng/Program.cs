﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using ImageMagick;
using System.Drawing;

namespace ConsoleEmfJpegJpgToPng
{
    class Program
    {
        static void Main(string[] args)
        {

            if (File.Exists("EmfJpegJpgToPng_error.log"))
            {
                File.Delete("EmfJpegJpgToPng_error.log");
            }

            string error;

           string[] ext = new string[3] { "*.emf", "*.jpeg", ".jpg" };

           foreach (string found in ext)
           {
               string[] extracted = Directory.GetFiles(Environment.CurrentDirectory + "\\docx", found, System.IO.SearchOption.AllDirectories);

               foreach (string file in extracted)
               {

                   try
                   {
                       using (MagickImage image = new MagickImage(file))
                       {
                           // Save frame as jpg
                           try
                           {
                               image.Write(Path.ChangeExtension(file, "png"));
                               Console.WriteLine("Convert " + file);
                           }
                           catch (Exception r)
                           {
                               error = "can't convert to png \r\n" + file + "\r\nException:" + r.Message + "\r\n";
                               File.AppendAllText("EmfJpegJpgToPng_error.log", error);
                               Console.WriteLine(error);
                               continue;
                           }
                       }

                       try
                       {
                           File.Delete(file);
                       }
                       catch(Exception q)
                       {
                           error = "can't Delete Converted File \r\n" + file + "\r\nException:" + q.Message + "\r\n";
                           File.AppendAllText("EmfJpegJpgToPng_error.log", error);
                           Console.WriteLine(error);
                           continue;
                       }
                   }
                   catch (Exception e)
                   {
                       error = "can't open file name \r\n" + file + "\r\nException:" + e.Message + "\r\n";
                       File.AppendAllText("EmfJpegJpgToPng_error.log", error);
                       Console.WriteLine(error);
                       continue;
                   }
               }
           }



        }
    }
}
