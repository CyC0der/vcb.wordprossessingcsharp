﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace ConsoleXmlBeautifier
{
    class Program
    {
        static void Main(string[] args)
        {
            
            if (File.Exists("XmlBeautifirer_error.log"))
            {
                File.Delete("XmlBeautifirer_error.log");
            }


             string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.xml", System.IO.SearchOption.AllDirectories);

            string error;

            foreach (string docx in list)
            {
                string Result;

                MemoryStream mStream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
                XmlDocument document = new XmlDocument();

                try
                {
                    // Load the XmlDocument with the XML.
                    document.Load(docx);
                    // document.lo
                    writer.Formatting = Formatting.Indented;

                    // Write the XML into a formatting XmlTextWriter
                    document.WriteContentTo(writer);
                    writer.Flush();
                    mStream.Flush();

                    // Have to rewind the MemoryStream in order to read
                    // its contents.
                    mStream.Position = 0;

                    // Read MemoryStream contents into a StreamReader.
                    StreamReader sReader = new StreamReader(mStream);

                    // Extract the text from the StreamReader.
                    String FormattedXML = sReader.ReadToEnd();

                    Result = FormattedXML;
                }
                catch (XmlException e)
                {
                            error = "can't beautiy  File name \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                            File.AppendAllText("XmlBeautifirer_error.log", error);
                            Console.WriteLine(error);
                            continue;
                }

                mStream.Close();
                writer.Close();

                File.WriteAllText(docx, Result);

                Console.WriteLine(docx + " XML Beautifuled!\r\n");
            }

        }
    }
}
