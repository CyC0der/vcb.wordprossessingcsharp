﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.IO.Packaging;
using System.Drawing;
using OpenXmlPowerTools;
using DocumentFormat.OpenXml.Packaging;


namespace ConsoleSimplifier
{
    class Program
    {


        static void Main(string[] args)
        {

            if (File.Exists("Simplifier_error.log"))
            {
                File.Delete("Simplifier_error.log");
            }

            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.docx", System.IO.SearchOption.AllDirectories);


            string error;

            foreach (string docx in list)
            {
                try
                {
                    using (WordprocessingDocument doc = WordprocessingDocument.Open(docx, true))
                    {
                        SimplifyMarkupSettings settings = new SimplifyMarkupSettings
                        {
                            RemoveComments = true,
                            RemoveContentControls = true,
                            RemoveEndAndFootNotes = true,
                            RemoveFieldCodes = false,
                            RemoveLastRenderedPageBreak = true,
                            RemovePermissions = true,
                            RemoveProof = true,
                            RemoveRsidInfo = true,
                            RemoveSmartTags = true,
                            RemoveSoftHyphens = true,
                            ReplaceTabsWithSpaces = false,

                            AcceptRevisions = false,
                            RemoveBookmarks = true,
                            RemoveGoBackBookmark = true

                        };

                        try
                        {
                            MarkupSimplifier.SimplifyMarkup(doc, settings);
                            Console.WriteLine("Simplify " + docx);
                        }
                        catch (Exception e)
                        {
                            error = "can't simplfiy File name \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                            File.AppendAllText("Simplifier_error.log", error);
                            Console.WriteLine(error);
                            continue;
                        }
                    }
                }
                catch (Exception er)
                {
                    error = "can't open File name \r\n" + docx + "\r\nException:" + er.Message + "\r\n";
                    File.AppendAllText("Simplifier_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }

            }
        }
    }
}
