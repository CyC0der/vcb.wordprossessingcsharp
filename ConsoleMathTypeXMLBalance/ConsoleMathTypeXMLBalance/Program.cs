﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ConsoleMathTypeXMLBalance
{
    class Program
    {
        static void Main(string[] args)
        {


            if (File.Exists("ReplaceXML_error.log"))
            {
                File.Delete("ReplaceXML_error.log");
            }


            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.xml", System.IO.SearchOption.AllDirectories);

            string error;

            foreach (string docx in list)
            {
                if (docx.IndexOf("riazi") != -1 || docx.IndexOf("Riazi") != -1)
                {
                    /* ساخت شی ایکس ام ال  */
                    XmlDocument xq = new XmlDocument();

                    /* بخاطر وجود دو نیم اسپیس که ما آنها را فراخوانی میکنیم نیاز است به شیوه زیر
                     * از منیجر استفاده شود و منیچر مورد نظر در طول استفاده از برنامه مورده استفاده قرار میگیرد*/

                    XmlNamespaceManager mq = new XmlNamespaceManager(xq.NameTable);
                    mq.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                    mq.AddNamespace("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
                     mq.AddNamespace("o", "urn:schemas-microsoft-com:office:office");

                    try
                    {
                        xq.Load(docx);
                    }
                    catch (Exception er)
                    {
                        //ErLogWrite("\r\n\r\n *** محتوای فایل سوال معتبر نیست \r\nFile: \r\n" + QFile + "\r\nException:" + er.Message + "\r\n");
                        return;
                    }

                    XmlNodeList OLEObj = xq.SelectNodes("//o:OLEObject[@ProgID=\"Equation.DSMT4\"]", mq);

                    if (OLEObj.Count != 0)
                    {
                        for(int i=0;i<OLEObj.Count;i++) {

                            if (OLEObj.Item(i).PreviousSibling.Attributes["style"] != null)
                            {
                                XmlNodeList rPr = OLEObj.Item(i).ParentNode.ParentNode.SelectNodes("w:rPr", mq);

                                if (rPr.Count != 0)
                                    rPr.Item(0).ParentNode.RemoveChild(rPr.Item(0));

                                   OLEObj.Item(i).ParentNode.ParentNode.PrependChild(xq.CreateNode(XmlNodeType.Element, "w:rPr", "http://schemas.openxmlformats.org/wordprocessingml/2006/main"));


                                XmlNode position = xq.CreateNode(XmlNodeType.Element, "w:position", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

                                XmlAttribute val = xq.CreateAttribute("w:val", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

                                Regex Height = new Regex("width:[0-9\\.]+pt;height:([0-9\\.]+)pt");

                                string stlye = OLEObj.Item(i).PreviousSibling.Attributes["style"].Value;

                    
                                Match M = Height.Match(stlye);
                                
                                if(M.Success) {

                                    decimal H = decimal.Parse(M.Groups[1].Value, CultureInfo.InvariantCulture);

                                    val.Value = "-" + (H - (26/4)).ToString().Replace("/",".");

                                }

                                position.Attributes.Append(val);

                                OLEObj.Item(i).ParentNode.ParentNode.SelectSingleNode("w:rPr", mq).AppendChild(position);

                                //Console.WriteLine(OLEObj.Item(i).ParentNode.ParentNode.OuterXml);

                            }
                        }
                        
                    }
                    Console.WriteLine(OLEObj.Count.ToString() + " " + docx);

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    // Save the document to a file and auto-indent the output.
                    XmlWriter writer = XmlWriter.Create(docx, settings);
                    xq.Save(writer);


                }

            }

           
        }
    }
}
