﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace Console8RemoveInFileAutomatic
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists("RemoveInFileAutomatic_error.log"))
            {
                File.Delete("RemoveInFileAutomatic_error.log");
            }


            string[] list = Directory.GetFiles(Environment.CurrentDirectory + "/docx", "*.xml", System.IO.SearchOption.AllDirectories);

            string error;

            Regex RgxRemoveUnsigned9 = new Regex("<w:sz.*/>");

            Regex RgxRemoveUnsigned8 = new Regex(" <w:szCs.*/>");

            Regex RgxRemoveUnsigned7 = new Regex("<w:rFonts.*/>");

            Regex RgxRemoveUnsigned6 = new Regex("<w:highlight.*/>");

            Regex RgxRemoveUnsigned5 = new Regex("<w:rtl />");

            Regex RgxRemoveUnsigned4 = new Regex("<w:ltr />");

            Regex RgxRemoveUnsigned3 = new Regex("<w:bCs />");

            Regex RgxRemoveUnsigned2 = new Regex("<w:b />");

            Regex RgxRemoveUnsigned1 = new Regex("<w:lang.*/>");

            Regex RgxRemove0 = new Regex("<w:bidi />");

            Regex RgxRemove0O1 = new Regex("<w:rPr>[\r\n\t ]*</w:rPr>");

            Regex RgxRemove0O2 = new Regex("<w:pPr>[\r\n\t ]*</w:pPr>");

            foreach (string docx in list)
            {
                string Result = "";

                try
                {
                    Result = File.ReadAllText(docx);
                }
                catch (Exception e)
                {
                    error = "can't Open fileName \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                    File.AppendAllText("RemoveInFileAutomatic_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }


                Result = RgxRemoveUnsigned9.Replace(Result, "");
                Result = RgxRemoveUnsigned8.Replace(Result, "");
                Result = RgxRemoveUnsigned7.Replace(Result, "");
                Result = RgxRemoveUnsigned6.Replace(Result, "");
                Result = RgxRemoveUnsigned5.Replace(Result, "");
                Result = RgxRemoveUnsigned4.Replace(Result, "");
                Result = RgxRemoveUnsigned3.Replace(Result, "");
                Result = RgxRemoveUnsigned2.Replace(Result, "");
                Result = RgxRemoveUnsigned1.Replace(Result, "");

                Result = RgxRemove0.Replace(Result, "");

                Result = RgxRemove0O1.Replace(Result, "");

                Result = RgxRemove0O2.Replace(Result, "");

                try
                {
                    File.WriteAllText(docx, Result);
                    Console.WriteLine(docx);
                }
                catch (Exception e)
                {
                    error = "can't save fileName \r\n" + docx + "\r\nException:" + e.Message + "\r\n";
                    File.AppendAllText("RemoveInFileAutomatic_error.log", error);
                    Console.WriteLine(error);
                    continue;
                }

            }
        }
    }
}
