﻿namespace ClearFormat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.start = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.chBUnderline = new System.Windows.Forms.CheckBox();
            this.chBBold = new System.Windows.Forms.CheckBox();
            this.chBltr = new System.Windows.Forms.CheckBox();
            this.txtFontName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.chBMathType = new System.Windows.Forms.CheckBox();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.label432 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.chBClose = new System.Windows.Forms.CheckBox();
            this.chBCostumNext = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFontSize = new System.Windows.Forms.NumericUpDown();
            this.groupBox87 = new System.Windows.Forms.GroupBox();
            this.btnWordOption = new System.Windows.Forms.Button();
            this.chBRemoveWordTmp = new System.Windows.Forms.CheckBox();
            this.txtDelay = new System.Windows.Forms.NumericUpDown();
            this.chBVisibleWord = new System.Windows.Forms.CheckBox();
            this.chBClearFormat = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lBScanned = new System.Windows.Forms.Label();
            this.lBTotal = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.selectPath = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.Reset = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lBPrecentView = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label6 = new System.Windows.Forms.Label();
            this.errorLog = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFontSize)).BeginInit();
            this.groupBox87.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Enabled = false;
            this.start.Location = new System.Drawing.Point(685, 270);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(89, 41);
            this.start.TabIndex = 0;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // stop
            // 
            this.stop.Enabled = false;
            this.stop.Location = new System.Drawing.Point(592, 270);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(87, 41);
            this.stop.TabIndex = 0;
            this.stop.TabStop = false;
            this.stop.Text = "Stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // chBUnderline
            // 
            this.chBUnderline.AutoSize = true;
            this.chBUnderline.Location = new System.Drawing.Point(16, 22);
            this.chBUnderline.Name = "chBUnderline";
            this.chBUnderline.Size = new System.Drawing.Size(126, 17);
            this.chBUnderline.TabIndex = 2;
            this.chBUnderline.Text = "Do not lose underline";
            this.chBUnderline.UseVisualStyleBackColor = true;
            // 
            // chBBold
            // 
            this.chBBold.AutoSize = true;
            this.chBBold.Location = new System.Drawing.Point(16, 46);
            this.chBBold.Name = "chBBold";
            this.chBBold.Size = new System.Drawing.Size(71, 17);
            this.chBBold.TabIndex = 3;
            this.chBBold.Text = "Font Bold";
            this.chBBold.UseVisualStyleBackColor = true;
            // 
            // chBltr
            // 
            this.chBltr.AutoSize = true;
            this.chBltr.Location = new System.Drawing.Point(16, 70);
            this.chBltr.Name = "chBltr";
            this.chBltr.Size = new System.Drawing.Size(79, 17);
            this.chBltr.TabIndex = 4;
            this.chBltr.Text = "Left to right";
            this.chBltr.UseVisualStyleBackColor = true;
            // 
            // txtFontName
            // 
            this.txtFontName.Location = new System.Drawing.Point(16, 134);
            this.txtFontName.Name = "txtFontName";
            this.txtFontName.Size = new System.Drawing.Size(134, 20);
            this.txtFontName.TabIndex = 5;
            this.txtFontName.Text = "B nazanin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Font Name:";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(13, 165);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(54, 13);
            this.label212.TabIndex = 7;
            this.label212.Text = "Font Size:";
            // 
            // chBMathType
            // 
            this.chBMathType.AutoSize = true;
            this.chBMathType.Location = new System.Drawing.Point(16, 93);
            this.chBMathType.Name = "chBMathType";
            this.chBMathType.Size = new System.Drawing.Size(106, 17);
            this.chBMathType.TabIndex = 9;
            this.chBMathType.Text = "MathType format";
            this.chBMathType.UseVisualStyleBackColor = true;
            this.chBMathType.CheckedChanged += new System.EventHandler(this.chBMathType_CheckedChanged);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(12, 30);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(481, 282);
            this.txtLog.TabIndex = 10;
            this.txtLog.Text = "";
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // label432
            // 
            this.label432.AutoSize = true;
            this.label432.Location = new System.Drawing.Point(12, 12);
            this.label432.Name = "label432";
            this.label432.Size = new System.Drawing.Size(54, 13);
            this.label432.TabIndex = 11;
            this.label432.Text = "Log View:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(595, 318);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(84, 13);
            this.label43.TabIndex = 12;
            this.label43.Text = "Ctrl+Q To STOP";
            // 
            // chBClose
            // 
            this.chBClose.AutoSize = true;
            this.chBClose.Location = new System.Drawing.Point(16, 72);
            this.chBClose.Name = "chBClose";
            this.chBClose.Size = new System.Drawing.Size(206, 17);
            this.chBClose.TabIndex = 13;
            this.chBClose.Text = "Save/Close docx manual ( Ctrl + End )";
            this.chBClose.UseVisualStyleBackColor = true;
            // 
            // chBCostumNext
            // 
            this.chBCostumNext.AutoSize = true;
            this.chBCostumNext.Location = new System.Drawing.Point(16, 96);
            this.chBCostumNext.Name = "chBCostumNext";
            this.chBCostumNext.Size = new System.Drawing.Size(206, 17);
            this.chBCostumNext.TabIndex = 14;
            this.chBCostumNext.Text = "Open next docx Manual ( Ctrl + Right )";
            this.chBCostumNext.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFontSize);
            this.groupBox1.Controls.Add(this.chBUnderline);
            this.groupBox1.Controls.Add(this.chBBold);
            this.groupBox1.Controls.Add(this.chBltr);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFontName);
            this.groupBox1.Controls.Add(this.label212);
            this.groupBox1.Controls.Add(this.chBMathType);
            this.groupBox1.Location = new System.Drawing.Point(781, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(163, 214);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ClearFormat/Format";
            // 
            // txtFontSize
            // 
            this.txtFontSize.Location = new System.Drawing.Point(16, 182);
            this.txtFontSize.Name = "txtFontSize";
            this.txtFontSize.Size = new System.Drawing.Size(59, 20);
            this.txtFontSize.TabIndex = 10;
            this.txtFontSize.Value = new decimal(new int[] {
            13,
            0,
            0,
            0});
            // 
            // groupBox87
            // 
            this.groupBox87.Controls.Add(this.btnWordOption);
            this.groupBox87.Controls.Add(this.chBRemoveWordTmp);
            this.groupBox87.Controls.Add(this.txtDelay);
            this.groupBox87.Controls.Add(this.chBVisibleWord);
            this.groupBox87.Controls.Add(this.chBClearFormat);
            this.groupBox87.Controls.Add(this.label3);
            this.groupBox87.Controls.Add(this.chBClose);
            this.groupBox87.Controls.Add(this.chBCostumNext);
            this.groupBox87.Controls.Add(this.label2);
            this.groupBox87.Location = new System.Drawing.Point(499, 55);
            this.groupBox87.Name = "groupBox87";
            this.groupBox87.Size = new System.Drawing.Size(276, 204);
            this.groupBox87.TabIndex = 16;
            this.groupBox87.TabStop = false;
            this.groupBox87.Text = "Operation";
            // 
            // btnWordOption
            // 
            this.btnWordOption.Location = new System.Drawing.Point(186, 22);
            this.btnWordOption.Name = "btnWordOption";
            this.btnWordOption.Size = new System.Drawing.Size(84, 22);
            this.btnWordOption.TabIndex = 27;
            this.btnWordOption.Text = "Options...";
            this.btnWordOption.UseVisualStyleBackColor = true;
            this.btnWordOption.Click += new System.EventHandler(this.btnWordOption_Click);
            // 
            // chBRemoveWordTmp
            // 
            this.chBRemoveWordTmp.AutoSize = true;
            this.chBRemoveWordTmp.Checked = true;
            this.chBRemoveWordTmp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBRemoveWordTmp.Location = new System.Drawing.Point(16, 48);
            this.chBRemoveWordTmp.Name = "chBRemoveWordTmp";
            this.chBRemoveWordTmp.Size = new System.Drawing.Size(150, 17);
            this.chBRemoveWordTmp.TabIndex = 21;
            this.chBRemoveWordTmp.Text = "Remove ~$ word temp file";
            this.chBRemoveWordTmp.UseVisualStyleBackColor = true;
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(16, 163);
            this.txtDelay.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(87, 20);
            this.txtDelay.TabIndex = 20;
            // 
            // chBVisibleWord
            // 
            this.chBVisibleWord.AutoSize = true;
            this.chBVisibleWord.Checked = true;
            this.chBVisibleWord.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBVisibleWord.Location = new System.Drawing.Point(16, 25);
            this.chBVisibleWord.Name = "chBVisibleWord";
            this.chBVisibleWord.Size = new System.Drawing.Size(121, 17);
            this.chBVisibleWord.TabIndex = 19;
            this.chBVisibleWord.Text = "Visible word window";
            this.chBVisibleWord.UseVisualStyleBackColor = true;
            this.chBVisibleWord.CheckedChanged += new System.EventHandler(this.chBVisibleWord_CheckedChanged);
            // 
            // chBClearFormat
            // 
            this.chBClearFormat.AutoSize = true;
            this.chBClearFormat.Location = new System.Drawing.Point(16, 120);
            this.chBClearFormat.Name = "chBClearFormat";
            this.chBClearFormat.Size = new System.Drawing.Size(257, 17);
            this.chBClearFormat.TabIndex = 18;
            this.chBClearFormat.Text = "Run Clearformat/Format manual ( Ctrl + PageUp )";
            this.chBClearFormat.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(109, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Millisecond";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Delay before auto Save/Close:";
            // 
            // lBScanned
            // 
            this.lBScanned.AutoSize = true;
            this.lBScanned.Location = new System.Drawing.Point(884, 83);
            this.lBScanned.Name = "lBScanned";
            this.lBScanned.Size = new System.Drawing.Size(13, 13);
            this.lBScanned.TabIndex = 23;
            this.lBScanned.Text = "0";
            // 
            // lBTotal
            // 
            this.lBTotal.AutoSize = true;
            this.lBTotal.Location = new System.Drawing.Point(884, 62);
            this.lBTotal.Name = "lBTotal";
            this.lBTotal.Size = new System.Drawing.Size(13, 13);
            this.lBTotal.TabIndex = 22;
            this.lBTotal.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(794, 83);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "Scanned Docx :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(794, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Total Docx :";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(499, 30);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(364, 20);
            this.txtFilePath.TabIndex = 17;
            this.txtFilePath.TextChanged += new System.EventHandler(this.txtFilePath_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(500, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Docx\'s File Location:";
            // 
            // selectPath
            // 
            this.selectPath.Location = new System.Drawing.Point(869, 28);
            this.selectPath.Name = "selectPath";
            this.selectPath.Size = new System.Drawing.Size(75, 23);
            this.selectPath.TabIndex = 19;
            this.selectPath.Text = "Select...";
            this.selectPath.UseVisualStyleBackColor = true;
            this.selectPath.Click += new System.EventHandler(this.selectPath_Click);
            // 
            // Reset
            // 
            this.Reset.Enabled = false;
            this.Reset.Location = new System.Drawing.Point(503, 270);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(83, 41);
            this.Reset.TabIndex = 24;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(44, 318);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(449, 23);
            this.progressBar1.TabIndex = 25;
            // 
            // lBPrecentView
            // 
            this.lBPrecentView.AutoSize = true;
            this.lBPrecentView.Location = new System.Drawing.Point(10, 323);
            this.lBPrecentView.Name = "lBPrecentView";
            this.lBPrecentView.Size = new System.Drawing.Size(21, 13);
            this.lBPrecentView.TabIndex = 26;
            this.lBPrecentView.Text = "0%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(796, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Erros:";
            // 
            // errorLog
            // 
            this.errorLog.AutoSize = true;
            this.errorLog.Location = new System.Drawing.Point(884, 103);
            this.errorLog.Name = "errorLog";
            this.errorLog.Size = new System.Drawing.Size(13, 13);
            this.errorLog.TabIndex = 28;
            this.errorLog.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 351);
            this.Controls.Add(this.errorLog);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.lBPrecentView);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.lBScanned);
            this.Controls.Add(this.selectPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lBTotal);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.groupBox87);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label432);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.start);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Docx Reviewer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFontSize)).EndInit();
            this.groupBox87.ResumeLayout(false);
            this.groupBox87.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.CheckBox chBUnderline;
        private System.Windows.Forms.CheckBox chBBold;
        private System.Windows.Forms.CheckBox chBltr;
        private System.Windows.Forms.TextBox txtFontName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.CheckBox chBMathType;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Label label432;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckBox chBClose;
        private System.Windows.Forms.CheckBox chBCostumNext;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox87;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chBClearFormat;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button selectPath;
        private System.Windows.Forms.CheckBox chBVisibleWord;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.NumericUpDown txtFontSize;
        private System.Windows.Forms.NumericUpDown txtDelay;
        private System.Windows.Forms.CheckBox chBRemoveWordTmp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lBTotal;
        private System.Windows.Forms.Label lBScanned;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lBPrecentView;
        private System.Windows.Forms.Button btnWordOption;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label errorLog;

    }
}

