﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClearFormat
{
    public partial class FromWordOptions : Form
    {
        private Form1 f;

        public FromWordOptions(Form1 f)
        {
            InitializeComponent();

            zoom.SelectedIndex = 0;
            this.f = f;
        }

        private void ok_Click(object sender, EventArgs e)
        {
            f.winHeight = height.Value;
            f.winWidth = width.Value;
            f.winLeft = left.Value;
            f.winTop = top.Value;
            f.winPageColumn = pageColumn.Value;
            f.winFullScreen = fullscreen.Checked;
            f.winZoomMode = zoom.SelectedIndex;

            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void zoom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FromWordOptions_Load(object sender, EventArgs e)
        {

        }


    }
}
