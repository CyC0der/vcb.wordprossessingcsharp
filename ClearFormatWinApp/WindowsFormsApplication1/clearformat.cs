﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Ionic.Zip;
using Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using Application1 = System.Windows.Forms.Application;
using Application2 = Microsoft.Office.Interop.Word.Application;



namespace ClearFormat
{
    public class clearformat
    {

        public Application2 app;
        public bool runing = true;
        private int TryCloseCount = 0;
        private int TrySaveCount = 0;
        private Form1 form;


        const int MAX_TRYING_SAVE = 2;
        const int MAX_TRYING_CLOSE = 2;

        public bool chBCostumNext;
        public bool chBVisibleWord;
        public decimal txtDelay;

        public clearformat(Form1 form,
            bool chBCostumNext,
            decimal txtDelay,
            bool chBVisibleWord
        ){

            this.form = form;
            this.chBVisibleWord = chBVisibleWord;
            this.chBCostumNext = chBCostumNext;
            this.txtDelay = txtDelay;
        }

        private void ClearFormat()
        {
            app.Selection.WholeStory();
            app.Selection.ClearFormatting();
            form.writeLog("- Ctrl+A  Clearformated\r\n");
        }

        private void ClearFormatAndSaveUnderline()
        {
            /* Ctrl+A */
            app.Selection.WholeStory();

            form.writeLog("- Ctrl+A\r\n");

            /* Set Selected text highlight to No Color */
            app.Selection.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;

            /* set default text highlight color to green */
            app.Options.DefaultHighlightColorIndex = WdColorIndex.wdBrightGreen;

            /* find all text with  underline and replace with highlighted text (green) */
            app.Selection.Find.ClearFormatting();
            app.Selection.Find.Font.Underline = WdUnderline.wdUnderlineSingle;
            app.Selection.Find.Replacement.ClearFormatting();
            app.Selection.Find.Replacement.Highlight = 1;
            app.Selection.Find.Text = "";
            app.Selection.Find.Replacement.Text = "";
            app.Selection.Find.Forward = true;
            app.Selection.Find.Wrap = WdFindWrap.wdFindContinue;
            app.Selection.Find.Format = true;
            app.Selection.Find.MatchCase = false;
            app.Selection.Find.MatchWholeWord = false;
            app.Selection.Find.MatchKashida = false;
            app.Selection.Find.MatchDiacritics = false;
            app.Selection.Find.MatchAlefHamza = false;
            app.Selection.Find.MatchControl = false;
            app.Selection.Find.MatchWildcards = false;
            app.Selection.Find.MatchSoundsLike = false;

            app.Selection.Find.Execute(
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                WdReplace.wdReplaceAll
            );

            form.writeLog("- Highlight text with underline\r\n");

            /* ClearFormat of Document text */
            this.ClearFormat();

            form.writeLog("- clearformated\r\n");

            /* find all text with  text highlighted and replace with underline text  */
            app.Selection.Find.ClearFormatting();
            app.Selection.Find.Highlight = 1;
            app.Selection.Find.Replacement.ClearFormatting();
            app.Selection.Find.Replacement.Font.Underline = WdUnderline.wdUnderlineSingle;
            app.Selection.Find.Text = "";
            app.Selection.Find.Replacement.Text = "";
            app.Selection.Find.Forward = true;
            app.Selection.Find.Wrap = WdFindWrap.wdFindContinue;
            app.Selection.Find.Format = true;
            app.Selection.Find.MatchCase = false;
            app.Selection.Find.MatchWholeWord = false;
            app.Selection.Find.MatchKashida = false;
            app.Selection.Find.MatchDiacritics = false;
            app.Selection.Find.MatchAlefHamza = false;
            app.Selection.Find.MatchControl = false;
            app.Selection.Find.MatchWildcards = false;
            app.Selection.Find.MatchSoundsLike = false;

            app.Selection.Find.Execute(
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                WdReplace.wdReplaceAll
            );

            form.writeLog("- Replace highlighted text with underlined text \r\n");

            /* remove background color */
            app.Options.DefaultHighlightColorIndex = WdColorIndex.wdNoHighlight;
            app.Selection.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;

            form.writeLog("- set text highlight to no color\r\n");
        }

        private void format(string FontName, decimal FontSize, bool isBold = false, bool isRTL = true)
        {
            if (isRTL)
            {
                form.writeLog("- Set RTL\r\n");
                app.Selection.LanguageID = WdLanguageID.wdPersian;
                app.Selection.RtlPara();
            }
            else
            {
                form.writeLog("- Set LTR\r\n");
                app.Selection.LtrPara();
            }

            form.writeLog("- Set Font name to " + FontName + " \r\n");

            app.Selection.Font.Name = FontName;

            if (chBVisibleWord)
            {
                app.Activate();
                form.writeLog("- Set FontSize top " + FontSize.ToString() + "\r\n");
                SendKeys.SendWait("%H");
                SendKeys.SendWait("fs");
                SendKeys.SendWait(FontSize.ToString());
                SendKeys.SendWait("{ENTER}");
                Thread.Sleep(500);
            }

            Thread.Sleep(100);

            if (isBold)
            {
                form.writeLog("- Set font weight bold\r\n");
                app.Selection.Font.Bold = 1;
                app.Selection.Font.BoldBi = 1;
            }

            /* Deselect textes By Set Cursor To Top */
            form.writeLog("- Set cursor to top\r\n");
            app.Selection.Collapse(WdCollapseDirection.wdCollapseStart);
        }

        private void TrySave()
        {
            if (TrySaveCount >= MAX_TRYING_SAVE)
            {
                form.ErLogWrite("\r\n~ " + TrySaveCount + " try for saving failed!\r\n\r\n");
                return;
            }

            try
            {
                TrySaveCount++;
                app.ActiveDocument.Save();
                form.writeLog("- Docx Saved\r\n");
            }
            catch
            {
                form.ErLogWrite("~ Can't save docx , trying(" + TrySaveCount + ")\r\n");
                Thread.Sleep(1000);
                this.TrySave();
            }
        }

        public void TryClose()
        {

            if (TryCloseCount >= MAX_TRYING_CLOSE)
            {
                form.ErLogWrite("\r\n~ " + TryCloseCount + " try for close failed!\r\n\r\n");
                return;
            }

            try
            {
                TryCloseCount++;
                app.ActiveWindow.Close();
                form.writeLog("- Docx Closed\r\n\r\n");
            }
            catch
            {
                form.ErLogWrite("~ Can't close docx , trying(" + TryCloseCount + ")\r\n");
                Thread.Sleep(1000);
                this.TryClose();
            }
        }

        public void StartClearFormating(
            bool chBUnderline, 
            string txtFontName, 
            decimal txtFontSize, 
            bool chBBold, 
            bool chBltr, 
            bool chBMathType
        ) {

            form.writeLog("- ClearFormating... \r\n");

            if (chBUnderline)
                this.ClearFormatAndSaveUnderline();
            else
                this.ClearFormat();

            this.format(txtFontName,txtFontSize,chBBold,chBltr);

            if (chBMathType)
            {
                app.Activate();
                form.writeLog("- Runing MathType Format... \r\n");
                SendKeys.SendWait("%y");
                SendKeys.SendWait("ye");
                SendKeys.SendWait("{ENTER}");
                Thread.Sleep(500);
            }
        }

        public void saveClose()
        {
            if (txtDelay > 0)
                Thread.Sleep(Convert.ToInt32(txtDelay));

            this.TrySave();
            this.TryClose();
        }

        public void OpenApplication()
        {
            app = new Application2();

            /* Visible Word Application Window if User checked Option */
            if (chBVisibleWord)
            {
                MessageBox.Show("WARNING:\r\n After closing this message word window apear \r\n Please focus on it and do not touch any thing! \r\n You can use Ctrl + Q to stop process");

                app.Visible = true;

                //app.Activate();

                Thread.Sleep(2000);
                return;
            }

            app.Visible = false;
        }

        public void removeTemp(FileInfo docx)
        {
            try
            {
                form.writeLog("** Trying to delete word tmp file\r\nname: " + docx.Name + " \r\n");
                File.Delete(docx.FullName);
                form.writeLog("Deleted. \r\n");
            }
            catch
            {
                form.ErLogWrite("~~ Can't Delete. \r\n");
            }
        }

        public bool OpenInitDocx(FileInfo docx)
        {
            Document doc = new Document();

            /* Reset Counter To 0 For Next Docx File */
            TryCloseCount = 0;
            TrySaveCount = 0;

            try
            {
                form.writeLog("\r\n** Opening file: \r\n" + docx.FullName + "\r\n");

                doc = app.Documents.Open(docx.FullName);
                form.writeLog("- Opened Success\r\n");
            }
            catch (Exception e)
            {

                form.ErLogWrite("~~ Opening failed!\r\n\tExeption Message:\r\n" +
                    e.Message + "\r\n"
                );
                return false;
            }

            if (doc.ReadOnly)
            {
                form.ErLogWrite("~~ Opening failed! , File is ReadOnly\r\n");
                app.ActiveWindow.Close();
                return false;
            }

            if (form.winFullScreen)
            {
                app.ActiveWindow.View.FullScreen = true;
                app.ActiveWindow.View.Zoom.PageColumns = Convert.ToInt32(form.winPageColumn);
            }
            else
            {
                app.ActiveWindow.Height = Convert.ToInt32(form.winHeight);
                app.ActiveWindow.Width = Convert.ToInt32(form.winWidth);
                app.ActiveWindow.Top = Convert.ToInt32(form.winTop);
                app.ActiveWindow.Left = Convert.ToInt32(form.winLeft);
                app.ActiveWindow.View.Zoom.PageColumns = Convert.ToInt32(form.winPageColumn);

                switch(form.winZoomMode) {
                    case 0:
                        app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitFullPage;
                    break;
                    case 1:
                         app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitBestFit;
                    break;
                    case 2:
                        app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitNone;
                    break;
                    case 3:
                        app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitTextFit;
                    break;
                }
 
            }

            doc.ShowGrammaticalErrors = false;
            doc.ShowRevisions = false;
            doc.ShowSpellingErrors = false;

            return true;
        }
        
        public void CloseApplication()
        {
            try
            {
                app.Quit(true);
            }
            catch
            {
                app = null;
            }
        }
    }
}
