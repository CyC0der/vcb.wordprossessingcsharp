﻿namespace ClearFormat
{
    partial class FromWordOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.left = new System.Windows.Forms.NumericUpDown();
            this.top = new System.Windows.Forms.NumericUpDown();
            this.height = new System.Windows.Forms.NumericUpDown();
            this.pageColumn = new System.Windows.Forms.NumericUpDown();
            this.width = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.fullscreen = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.zoom = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Offset Left:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Page Columns:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Offset Top:";
            // 
            // left
            // 
            this.left.Location = new System.Drawing.Point(15, 36);
            this.left.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(120, 20);
            this.left.TabIndex = 4;
            this.left.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // top
            // 
            this.top.Location = new System.Drawing.Point(15, 85);
            this.top.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(120, 20);
            this.top.TabIndex = 5;
            this.top.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(151, 85);
            this.height.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(120, 20);
            this.height.TabIndex = 6;
            this.height.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // pageColumn
            // 
            this.pageColumn.Location = new System.Drawing.Point(151, 135);
            this.pageColumn.Name = "pageColumn";
            this.pageColumn.Size = new System.Drawing.Size(120, 20);
            this.pageColumn.TabIndex = 7;
            this.pageColumn.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(151, 36);
            this.width.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(120, 20);
            this.width.TabIndex = 8;
            this.width.Value = new decimal(new int[] {
            730,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(148, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Width:";
            // 
            // fullscreen
            // 
            this.fullscreen.AutoSize = true;
            this.fullscreen.Location = new System.Drawing.Point(15, 170);
            this.fullscreen.Name = "fullscreen";
            this.fullscreen.Size = new System.Drawing.Size(79, 17);
            this.fullscreen.TabIndex = 11;
            this.fullscreen.Text = "FullScreen:";
            this.fullscreen.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Height:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(196, 202);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(114, 202);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 14;
            this.ok.Text = "Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Zoom Mode:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // zoom
            // 
            this.zoom.DisplayMember = "Fit full page";
            this.zoom.Items.AddRange(new object[] {
            "Fit Full page",
            "Fit Best ",
            "Fit Default",
            "Fit Text"});
            this.zoom.Location = new System.Drawing.Point(15, 134);
            this.zoom.Name = "zoom";
            this.zoom.Size = new System.Drawing.Size(121, 21);
            this.zoom.TabIndex = 9;
            this.zoom.SelectedIndexChanged += new System.EventHandler(this.zoom_SelectedIndexChanged);
            // 
            // FromWordOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 234);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.fullscreen);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.zoom);
            this.Controls.Add(this.width);
            this.Controls.Add(this.pageColumn);
            this.Controls.Add(this.height);
            this.Controls.Add(this.top);
            this.Controls.Add(this.left);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FromWordOptions";
            this.Text = "Word Window";
            this.Load += new System.EventHandler(this.FromWordOptions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.top)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown left;
        private System.Windows.Forms.NumericUpDown top;
        private System.Windows.Forms.NumericUpDown height;
        private System.Windows.Forms.NumericUpDown pageColumn;
        private System.Windows.Forms.NumericUpDown width;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox fullscreen;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox zoom;
    }
}