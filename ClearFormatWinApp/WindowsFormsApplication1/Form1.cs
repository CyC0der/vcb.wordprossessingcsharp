﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;

namespace ClearFormat
{

    public partial class Form1 : Form
    {
        public clearformat cf;

        public string[] DocxList;
        public int DocxListPointer = 0;

        const int GLOBALKEY_STOP_WORD_PROSSESS = 1;
        const int GLOBALKEY_OPEN_NEXT = 2;
        const int GLOBALKEY_SAVE_CLOSE = 3;
        const int GLOBALKEY_CLEARFORMAT = 4;

        public decimal winWidth = 730;
        public decimal winHeight = 200;
        public decimal winLeft = 10;
        public decimal winTop  = 10;
        public decimal winPageColumn = 5;
        public bool winFullScreen = false;
        public int winZoomMode = 0;
        public int ErrorCounter = 0;

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public Form1()
        {
            InitializeComponent();

            if (File.Exists("ClearFormat_errors.log"))
                File.Delete("ClearFormat_errors.log");

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            /* Binding events to the  background worker */
            /* code to run */
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            /* invloked when proccess end */
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            /* invoked when worker.ReportProgress() used in  doWork */
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;

            if (File.Exists("conf.ini"))
                txtFilePath.Text = System.IO.File.ReadAllText("conf.ini");

            // Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8  ( ALT+CTRL = 1 + 2 = 3 )

            /* Ctrl + Q   stop word proccess */
            RegisterHotKey(this.Handle, GLOBALKEY_STOP_WORD_PROSSESS, 2, (int)Keys.Q);

            /* Ctrl + Right */
            RegisterHotKey(this.Handle, GLOBALKEY_OPEN_NEXT, 2, (int)Keys.Right);

            /* Ctrl + end */
            RegisterHotKey(this.Handle, GLOBALKEY_SAVE_CLOSE, 2, (int)Keys.End);

            /* Ctrl + PageUp */
            RegisterHotKey(this.Handle, GLOBALKEY_CLEARFORMAT, 2, (int)Keys.PageUp);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312) {
                   int keyCode = m.WParam.ToInt32();
                    switch(keyCode) {

                        case GLOBALKEY_STOP_WORD_PROSSESS:                
                            writeLog("\r\n\r\n** Plase wait... Trying to stop proccess \r\n");
                            if (chBCostumNext.Checked)
                            {
                                cf.CloseApplication();
                                stopProcess();
                            }

                            if (backgroundWorker1.WorkerSupportsCancellation == true)
                            {
                                backgroundWorker1.CancelAsync();
                            }
                         break;

                        case GLOBALKEY_OPEN_NEXT:
                         if (chBCostumNext.Checked)
                         {
                             if (!chBClose.Checked)
                             {
                                 cf.saveClose();
                             }
                             else
                             {
                                 cf.TryClose();
                             }
                             writeLog("\r\n\r\n** Opening next docx... \r\n");
                             goToNext();
                         }
                         break;

                        case GLOBALKEY_SAVE_CLOSE:
                         if (chBClose.Checked)
                         {
                             writeLog(" ** Save and closing current docx...\r\n");
                             cf.saveClose();
                         }
                         break;

                        case GLOBALKEY_CLEARFORMAT:
                         if (chBClearFormat.Checked)
                         {
                             writeLog(" ** clearformating current docx...\r\n");
                             cf.StartClearFormating(
                                chBUnderline.Checked,
                                txtFontName.Text,
                                txtFontSize.Value,
                                chBBold.Checked,
                                !chBltr.Checked,
                                chBMathType.Checked
                            );
                         }
                         break;

                        default:
                            break;
                    }
            }

            base.WndProc(ref m);
        }

        public void goToNext()
        {

            if (DocxListPointer != DocxList.Length - 1)
            {
                DocxListPointer++;

                run_once();
            }
        }

        public void writeLog(string log)
        {
            /* دلیل استفاده از این متد این است که دسترسی و وارد کردن مقدار از داخل توابع ورکر به کنتزل های فرم به شکل معمولی ممکن نیست*/
            this.Invoke(new MethodInvoker(delegate
            {
                // Execute the following code on the GUI thread.
                txtLog.Text += log;
            }));
        }

        public void ErLogWrite(string log)
        {
            writeLog(log);

            using (StreamWriter sw = File.AppendText("ClearFormat_errors.log"))
            {
                sw.WriteLine(log);
            }

            ErrorCounter++;

            this.Invoke(new MethodInvoker(delegate
            {
                errorLog.Text = ErrorCounter.ToString();
            }));
        }

        private void workingUIMode()
        {
            stop.Enabled = true;
            Reset.Enabled = false;
            start.Enabled = false;
            txtFilePath.Enabled = false;
            selectPath.Enabled = false;
        }

        private void freeUIMode()
        {
            Reset.Enabled = true;
            start.Enabled = true;
            txtFilePath.Enabled = true;
            selectPath.Enabled = true;
            stop.Enabled = false;
        }

        public void resetDocxList()
        {
            start.Enabled = false;
            lBTotal.Text = "0";
            lBScanned.Text = "0";
            DocxList = null;
            DocxListPointer = 0;
        }

        /*  این متد صحت آدرس انتخاب شده و اینکه آیا فایل ورد در آن است یا نه را چک کرده و لیست فایل های ورد را داخل یک آرایه رشته میریزد*/
        public bool fetchDocxList()
        {

            string[] list;

            resetDocxList();

            if (!Directory.Exists(txtFilePath.Text))
            {
                writeLog("** selected directory not exists \r\n\r\n");
                start.Enabled = false;
                return false;
            }

            try
            {
                list = System.IO.Directory.GetFiles(txtFilePath.Text, "*.docx", System.IO.SearchOption.AllDirectories);
            }
            catch (Exception er)
            {
                writeLog("** can't access to selected directory! \r\n\r\n");
                start.Enabled = false;
                return false;
            }


            lBTotal.Text = list.Length.ToString();

            if (list.Length != 0)
            {
                System.IO.File.WriteAllText("2conf.ini", txtFilePath.Text);

                this.DocxList = list;

                start.Enabled = true;

                return true;
            }

            start.Enabled = false;
            return false;
        }
        
        private void stopProcess() {
            writeLog("\r\n\t **** Stoped! ****\r\n\r\n");
            freeUIMode();
        }
        
        private void start_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                if (chBVisibleWord.Checked)
                {
                    this.Location = new Point(20, 300);
                }

                cf = new clearformat(this, chBCostumNext.Checked, txtDelay.Value, chBVisibleWord.Checked);

                writeLog("\r\n\t **** Start Converting ! ****\r\n\r\n");

                workingUIMode();

                if (chBCostumNext.Checked)
                {
                    cf.OpenApplication();
                    goToNext();
                    return;
                }

                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void stop_Click(object sender, EventArgs e)
        {
            if (chBCostumNext.Checked)
            {
                cf.CloseApplication();
            }

            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                backgroundWorker1.CancelAsync();
            }

            freeUIMode();
        }

        private void selectPath_Click(object sender, EventArgs e)
        {
            /* if user click on OK then */
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFilePath.Text = folderBrowserDialog1.SelectedPath;
                resetDocxList();
            }
        }

        private void chBVisibleWord_CheckedChanged(object sender, EventArgs e)
        {
            if (chBVisibleWord.Checked)
            {
                txtFontSize.Enabled = true;
                chBMathType.Enabled = true;
                chBCostumNext.Enabled = true;
                chBClose.Enabled = true;
                chBClearFormat.Enabled = true;
                btnWordOption.Enabled = true;
            }
            else
            {
                txtFontSize.Enabled = false;
                chBMathType.Checked = false;
                chBMathType.Enabled = false;
                chBCostumNext.Enabled = false;
                chBClose.Enabled = false;
                chBClearFormat.Enabled = false;
                chBCostumNext.Checked = false;
                chBClose.Checked = false;
                chBClearFormat.Checked = false;
                btnWordOption.Enabled = false;

                txtFontSize.Value = 11;
               MessageBox.Show("Important notice about invisible mode :\r\n\r\n"+
                   "\t1. Font size not working \r\n" + 
                   "\t2. MathType balance function not working");
            }
            
        }

        private void chBMathType_CheckedChanged(object sender, EventArgs e)
        {
            if (chBMathType.Checked)
            {
                chBCostumNext.Checked = true;
                chBCostumNext.Enabled = false;
                chBClose.Checked = true;
                chBClose.Enabled = false;
            }
            else
            {
                chBCostumNext.Checked = false;
                chBCostumNext.Enabled = true;
                chBClose.Checked = false;
                chBClose.Enabled = true;
            }

            if(chBMathType.Checked)
                MessageBox.Show("Notice :\r\n\r\n When MathType balance checked \r\n user have to go to next docx manualy \r\n by using Ctrl + Right");
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {

            txtLog.SelectionStart = txtLog.Text.Length; //Set the current caret position at the end
            txtLog.ScrollToCaret(); //Now scroll it automatically
        }

        private void txtFilePath_TextChanged(object sender, EventArgs e)
        {
            resetDocxList();

            if (!Directory.Exists(txtFilePath.Text))
                return;

            fetchDocxList();
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            if (File.Exists("ClearFormat_errors.log"))
                File.Delete("ClearFormat_errors.log");

            freeUIMode();
            DocxListPointer = 0;
            progressBar1.Value = 0;
            lBPrecentView.Text = "0%";
            lBScanned.Text = "0";
            Reset.Enabled = false;
        }

        private void run_once()
        {
            FileInfo docx = new FileInfo(DocxList[DocxListPointer]);

            if (chBRemoveWordTmp.Checked && docx.FullName.IndexOf('$') != -1)
            {
                cf.removeTemp(docx);
                return;
            }

            if (!chBVisibleWord.Checked)
                cf.OpenApplication();

            if (!cf.OpenInitDocx(docx))
                return;

            if (chBMathType.Checked)
                cf.app.Run("MathTypeCommands.UIEnableDisable.UIUpdate");

            /* Prevent auto ClearFormat if Manual Option was enabled */
            if (!chBClearFormat.Checked)
                cf.StartClearFormating(
                    chBUnderline.Checked,
                    txtFontName.Text,
                    txtFontSize.Value,
                    chBBold.Checked,
                    !chBltr.Checked,
                    chBMathType.Checked
                );

            /* Prevent auto Save/Close if Manual Option was enabled */
            if (!chBClose.Checked)
                cf.saveClose();

            if (!chBVisibleWord.Checked)
                cf.CloseApplication();

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            int dLen = DocxList.Length;

            cf.OpenApplication();

            for (; DocxListPointer < dLen; DocxListPointer++)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                run_once();


                worker.ReportProgress(Convert.ToInt32(Math.Round(DocxListPointer * 100 / (decimal)dLen)));
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar1.Value = e.ProgressPercentage;

            lBPrecentView.Text = e.ProgressPercentage.ToString() + "%";

            lBScanned.Text = (DocxListPointer + 1).ToString();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
             cf.CloseApplication();

            if (!e.Cancelled)
            {
                writeLog("\r\n\t **** Finished! :) ****\r\n\r\n");
                DocxListPointer = 0;
                freeUIMode();
                return;
            }

            stopProcess();
        }

        private void btnWordOption_Click(object sender, EventArgs e)
        {
            FromWordOptions optWin = new FromWordOptions(this);
            optWin.Show();
        }
    }
}
